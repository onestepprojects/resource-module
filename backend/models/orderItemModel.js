import mongoose from 'mongoose';
import { locSchema, ShippingOptions, OrderState } from './utilSchemas.js';

/**
  Models an ordered item in the OneStep Resource module.
*/

const orderItemSchema = new mongoose.Schema({
  // order item starts with a resource & qty from which to calc total cost
  resource: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Resource',
    required: true
  },
  qty: { type: Number, required: true },
  orderState: { type: String, enum: OrderState, default: "CREATED", required: true },

  // shipping/pickup details
  shippingOption: { type: String, enum: ShippingOptions, required: true },
  shipToLoc: locSchema,

  // these fields are set by service
  totalCost: { type: Number, required: false },
  personId: { type: String, default: "PERSON ID NOT SET", required: true },     // OneStep Person ID
  supplierId: { type: String, default: "SUPPLIER ID NOT SET", required: true }, // internal supplier ID
  orgId: { type: String, default: "ORG ID NOT SET", required: true },          // org module ID

  // these fields are set post-payment
  paymentMethod: { type: String, required: false },
  txId: { type: String, required: false },
});

const orderItemModel = mongoose.model('OrderItem', orderItemSchema);
export default orderItemModel;
