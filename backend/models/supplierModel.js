import mongoose from 'mongoose';
import { reviewSchema, locSchema, ShippingOptions } from './utilSchemas.js';

/**
  Models a Supplier in the OneStep Relief Platform Resource Module. Most characteristics
  are tied to the supplier organization via organization ID. This model builds on the
  "base" organizations with fields specific to the Resource Module.
*/

/**
  Supplier model.
*/
const supplierSchema = new mongoose.Schema({
  orgId: { type: String, required: true },
  rating: { type: Number, default: 0, required: true },
  numReviews: { type: Number, default: 0, required: true },
  reviews: [reviewSchema]
});

const supplierModel = mongoose.model('Supplier', supplierSchema);
export default supplierModel;
