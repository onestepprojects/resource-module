import mongoose from 'mongoose';
import { reviewSchema, locSchema, ShippingOptions, ResourceVisibility, Currency } from './utilSchemas.js';

/**
  Models a Resource in the OneStep Relief Platform Resource Module.
  Resources could be a product, service, rental, etc.
*/

const resourceSchema = new mongoose.Schema({
  // required fields
  name: { type: String, required: true },
  description: { type: String, required: true },
  category: { type: String, required: true },
  price: { type: Number, default: 0, required: true },
  currency: { type: String, default: "USDC", enum: Currency, required: true },
  countInStock: { type: Number, default: 0, required: true },
  rating: { type: Number, default: 0, required: true },
  numReviews: { type: Number, default: 0, required: true },
  views: { type: Number, default: 0, required: true },
  reviews: [reviewSchema],
  location: locSchema,
  visibility: { type: String, enum: ResourceVisibility, default: 'UNPUBLISHED', required: true },
  shippingOption: { type: String, enum: ShippingOptions, default: 'PICKUP', required: true },

  // optional fields
  creatorId: { type: String, required: false },  // onestep person ID
  supplierId: { type: String, required: false }, // internal supplier ID
  orgId: { type: String, required: false },      // onestep org ID
  productCode: { type: String, required: false },
  image: { type: String, required: false },
  brand: { type: String, required: false },
  note: {type: String, required: false },
  condition: { type: String, enum: ['NEW', 'USED'], default: 'NEW', required: false },
});

const resourceModel = mongoose.model('Resource', resourceSchema);
export default resourceModel;
