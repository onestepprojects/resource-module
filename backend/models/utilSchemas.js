import mongoose from 'mongoose';

/**
  Utility schemas for more complex parts of Resource documents.
  These schemas do not have their own collections in MongoDB (except for reviews), but allow
  nesting of multi-field properties like location or reviews.
*/

/**
  Valid values for shipping options.
*/
const ShippingOptions = [ 'PICKUP', 'DELIVERY' ];

/**
  Valid options for resource visibility.
*/
const ResourceVisibility = [ 'PUBLISHED', 'UNPUBLISHED' ];

/**
  Order state
*/
const OrderState = [ 'CREATED', 'PAID', 'SHIPPED', 'AWAITING_PICKUP', 'COMPLETED', 'CANCELLED' ];

/**
  Currency
*/
const Currency = [ 'ALGO', 'USDC' ];

/**
  Represents a location. Requires lat/long.
*/
const locSchema = new mongoose.Schema(
  {
    lat: { type: Number, required: false },
    long: { type: Number, required: false },
    address: { type: String, required: false },
    city: { type: String, required: false },
    postalCode: { type: String, required: false },
    country: { type: String, required: false}
  },
  { _id: false }
);

/**
  Represents a review--could be used for a resource or supplier.
*/
const reviewSchema = new mongoose.Schema(
  {
    personId: { type: String, required: false },
    name: { type: String, required: false },
    rating: { type: Number, default: 0 },
    comment: { type: String, required: true },
  },
  {
    timestamps: true,
  }
);

export { ShippingOptions, reviewSchema, locSchema, ResourceVisibility, OrderState, Currency };
