import OrderItem from '../models/orderItemModel.js';
import Resource from '../models/resourceModel.js';
import Supplier from '../models/supplierModel.js';
import { checkSupplierOrgRoles, checkGlobalAdmin } from './authService.js';
import { getResource } from './resourceService.js';
import { idPersonByAuthToken, getOrg } from '../clients/org/org-client.js';
import { AuthServiceError, ResourceServiceError, SupplierServiceError, OrderServiceError } from '../errors/errors.js';
import { sendAlgo, sendUsdc } from '../clients/blockchain/blockchain-client.js'
import mongoose from 'mongoose';

/**
  Service-level methods for Order CRUD and other functionality in the OneStep
  Relief platform Resource module. This layer sits between ExpressJS (REST)
  and Mongoose (MongoDB) and is designed to encapsulate service-level logic.
*/


/**
  Gets a collection of orders based on (optional) filter parameters.

  @param supplierId optional resource module-specific supplier ID filter
  @param orgId optional org module ID filter
  @param personId optional person ID filter
  @param searchKeyword optional search keyword filter
  @param sortOrder optional sort order
  @return list of orders based on criteria, can be empty
*/
async function getOrderItems(supplierId, orgId, personId, searchKeyword, sortOrder) {
  const orderItems = await OrderItem.find({ ...supplierId, ...orgId, ...personId, ...searchKeyword }).sort(sortOrder);
  return orderItems;
}

/**
  Gets an order based on ID. Returns null if not found.

  @param id ID of order to retrieve
  @return OrderItem or null
  @throws OrderServiceError if ID format is invalid
*/
async function getOrderItem(id) {
  if (mongoose.Types.ObjectId.isValid(id)) {
    const order = await OrderItem.findOne({ _id: id });
    return order;
  }
  throw new OrderServiceError("Invalid order ID format");
}

/**
  Creates a new order.

  @param orderItem order to create
  @param authToken Bearer <token>
  @return newly created order
  @throws ResourceServiceError if resource ID is not found/invalid
  @throws OrderService if unable to create new order
*/
async function createOrderItem(orderItem, authToken) {
  try {
    // first need to ensure resource exists
    const resource = await getResource(orderItem.resource);
    if (resource == null) {
      throw new ResourceServiceError("Could not find resource ID specified in new order");
    }

    // if out of stock, can't place order
    if (resource.countInStock < orderItem.qty) {
      throw new OrderServiceError("Cannot place order--insufficient count in stock");
    }

    // get person ID to set person
    const personResponse = await idPersonByAuthToken(authToken);
    if (personResponse == null) {
      throw new OrderServiceError("Could not retrieve person from org service via auth token");
    }
    orderItem.personId = personResponse.uuid;

    // calc and set order details
    orderItem.totalCost = orderItem.qty * resource.price;
    orderItem.supplierId = resource.supplierId;
    orderItem.orgId = resource.orgId;

    const newOrderItem = await orderItem.save();

    if (newOrderItem) {
      return newOrderItem;
    }

    throw new OrderServiceError("Error in creating order");
  }
  catch (err) {
    console.log(err);
    throw err;
  }
}

/**
  Updates an order state by ID.

  @param id order ID to update
  @param newOrderState new order state
  @param authToken Bearer <token>
  @return updated order
  @throws OrderServiceError if update fails
*/
async function updateOrderItemState(id, newOrderState, authToken) {
  try {
    console.log("Attempting to update order state for ID " + id + " to " + newOrderState);

    // find order
    if (!mongoose.Types.ObjectId.isValid(id)) {
      throw new OrderServiceError("Invalid order ID format");
    }

    const orderToUpdate = await OrderItem.findById(id);
    if (orderToUpdate == null) {
      throw new OrderServiceError("Could not find order item ID");
    }

    // authorize - depends on state--
    //  SUPPLIERS can change state from PAID -> SHIPPED if shippingOption = DELIVERY, or can change to CANCELLED at any time
    //  CUSTOMERS can change state from AWAITING_PICKUP or SHIPPED -> COMPLETED once they receive their order

    // can only change to the following states
    const allowedStates = [ "CANCELLED", "SHIPPED", "COMPLETED" ];
    if (!allowedStates.includes(newOrderState)) {
      throw new OrderServiceError("Illegal to set order state: " + newOrderState);
    }

    // suppliers
    if (newOrderState === "CANCELLED" ||
        (newOrderState === "SHIPPED" && orderToUpdate.orderState === "PAID" && orderToUpdate.shippingOption === "DELIVERY")) {
      console.log("supplier attempting to set state to: " + newOrderState);

      const accessCheck = await checkSupplierOrgRoles(authToken, orderToUpdate.orgId);
      orderToUpdate.orderState = newOrderState;
    }

    // customers
    if (newOrderState === "COMPLETED" && (orderToUpdate.orderState === "AWAITING_PICKUP" || orderToUpdate.orderState === "SHIPPED")) {
      console.log("customer attempting to set state to: " + newOrderState);

      // get person ID from auth token
      const personResponse = await idPersonByAuthToken(authToken);
      if (personResponse == null) {
        throw new OrderServiceError("Could not retrieve person from org service via auth token");
      }

      if (orderToUpdate.personId === personResponse.uuid) {
        orderToUpdate.orderState = newOrderState;
      }
    }

    const updatedOrder = orderToUpdate.save();
    if (updatedOrder) {
      return updatedOrder;
    }

    throw new OrderServiceError("Error while updating order state");
  }
  catch (err) {
    console.log(err);
    throw err;
  }
}

/**
  Deletes an order by ID. As of 4/28/21, only a "GLOBAL_ADMIN" can delete an order.

  TODO: Not tested, as didn't have reliable access to a global admin account during final sprint.
        This call is not accessible via UI as of 5/2/21.

  @param id ID of order to delete
  @param authToken Bearer <token>
  @return deleted order
  @throws OrderServiceError if delete fails
*/
async function deleteOrderItem(id, authToken) {
  // find order
  if (!mongoose.Types.ObjectId.isValid(id)) {
    throw new OrderServiceError("Invalid order ID format");
  }

  const orderToDelete = await OrderItem.findById(id);
  if (orderToDelete == null) {
    throw new OrderServiceError("Could not find order ID");
  }

  // authorize -- only allowed by global admins
  try {
    const accessCheck = await checkGlobalAdmin(authToken);
  }
  catch (err) {
    throw err;
  }

  // delete
  const res = await orderToDelete.remove();

  return orderToDelete;
}

/**
  Pays an order.

  @param orderItemId order ID to pay
  @param sender sender's payment information
  @param authToken bearer token
  @return completed blockchain transaction
  @throws OrderServiceError if problem with order
  @throws SupplierServiceError if unable to retrieve supplier attached to order
  @throws ResourceServiceError if unable to retrieve resource attached to order
  @throws AuthServiceError if person is unauthorized to pay order
*/
async function payOrderItem(orderItemId, sender, authToken) {
  console.log('paying order...');

  // find order
  if (!mongoose.Types.ObjectId.isValid(orderItemId)) {
    console.log(orderItemId);
    return { message: 'Order Item ID invalid' };
  }
  const orderItem = await OrderItem.findById(orderItemId);
  if (orderItem == null) {
    throw new OrderServiceError("Could not find order ID");
  }

  // get person ID from auth token
  // TODO: do we actually care about this? why would it matter who pays for the order?
  const personResponse = await idPersonByAuthToken(authToken);
  if (personResponse == null) {
    throw new OrderServiceError("Could not retrieve person from org service via auth token");
  }
  if (orderItem.personId != personResponse.uuid) {
    throw new AuthServiceError("This person is not authorized to pay this order");
  }

  // find supplier
  if (!mongoose.Types.ObjectId.isValid(orderItem.supplierId)) {
    return new SupplierServiceError("Supplier ID attached to order is invalid");
  }
  const supplier = await Supplier.findById(orderItem.supplierId);
  if (supplier == null) {
    return new SupplierServiceError("Could not find supplier ID attached to this order");
  }

  // find resource
  if (!mongoose.Types.ObjectId.isValid(orderItem.resource)) {
    return new ResourceServiceError("Resource ID attached to order is invalid");
  }
  const resource = await Resource.findById(orderItem.resource);
  if (resource == null) {
    return new ResourceServiceError("Could not find resource ID attached to this order");
  }

  // prepare payment data

  // get org for supplier, and use the org's paystring or address, whichever is available
  const org = await getOrg(supplier.orgId);
  if (org == null) {
    throw new OrderServiceError("Unable to retrieve organization info for supplier so cannot retrieve payment information");
  }
  const receiver = { address: null };
  if (org.paymentAccount.payID != null) {
    receiver.address = org.paymentAccount.payID;
  }
  else if (org.paymentAccount.blockchainAddress != null) {
    receiver.address = org.paymentAccount.blockchainAddress;
  }
  else {
    throw new OrderServiceError("Organization does not have a valid pago and/or blockchain account to receive payment");
  }

  const note = "OneStep resource module payment for order ID: " + orderItem._id;

  // execute transaction based on currency
  var response = null;
  if (resource.currency === "ALGO") {
    response = await sendAlgo(sender, receiver, orderItem.totalCost, note);
    await processPaid(orderItem, resource); // change order state and decrement in stock
    return response;
  }
  else if (resource.currency === "USDC") {
    response = await sendUsdc(sender, receiver, orderItem.totalCost, note);
    await processPaid(orderItem, resource); // change order state and decrement in stock
    return response;
  }
  else {
    throw new OrderServiceError("The resource's currency type is not supported");
  }

  throw new OrderServiceError("Unable to execute transaction -- blockchain request failed");
}

/**
  Helper function to set order state to paid & decrement in-stock.
*/
async function processPaid(orderItem, resource) {
  orderItem.orderState = "PAID";
  resource.countInStock = resource.countInStock - orderItem.qty;

  const updatedOrder = await orderItem.save();
  if (updatedOrder == null) {
    throw new OrderServiceError("Error updating order state after payment. Please contact supplier");
  }

  const updatedResource= await resource.save();
  if (updatedResource == null) {
    throw new ResourceServiceError("Error updating resource count in stock after payment. Please contact supplier");
  }
}

export { getOrderItems, getOrderItem, updateOrderItemState, deleteOrderItem, createOrderItem, payOrderItem };
