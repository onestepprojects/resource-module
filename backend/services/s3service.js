import AWS from 'aws-sdk';

/**
  Provides S3 upload for the OneStep Resource module.
*/

const ID = process.env.ONESTEP_RESOURCE_S3_ID;
const SECRET = process.env.ONESTEP_RESOURCE_S3_SECRET;
const BUCKET = process.env.ONESTEP_RESOURCE_S3_BUCKET;

const s3 = new AWS.S3({
    accessKeyId: ID,
    secretAccessKey: SECRET
});

/**
  Upload a file and return URL.

  @param id ID for file
  @param fileData base64 encoded file data
  @return URL of file or null on fail (though should throw err)
  @throws error from s3
*/
export async function uploadFile(id, fileData, callback) {
  // decode from base64
  let buffer = new Buffer(fileData.data, 'base64');

  // prepare & execute upload
  const params = {
      Bucket: BUCKET,
      Key: id + fileData.name,
      Body: buffer
  };

  try {
    console.log("attempting upload to s3 on backend: " + fileData.name);

    const data = await s3.upload(params).promise();
    return data.Location;
  }
  catch (err) {
    console.log(err);
    throw err;
  }
}
