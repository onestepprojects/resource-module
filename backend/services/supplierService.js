import Supplier from '../models/supplierModel.js';
import { checkSupplierOrgRoles } from './authService.js';
import { idPersonByAuthToken, getPersonOrgRoles, getOrg } from '../clients/org/org-client.js';
import { AuthServiceError, SupplierServiceError } from '../errors/errors.js';

import mongoose from 'mongoose';

/**
  Service layer for Suppliers. Note that ExpressJS verifies the auth token itself;
  this service only cares about authorization of the person.
*/

/**
  Gets all suppliers.

  @return list of all suppliers
*/
async function getSuppliers() {
  const suppliers = await Supplier.find();
  return suppliers;
}

/**
  Gets an supplier based on internal ID (e.g. resource module assigned). Returns null if not found.

  @param id resource module-specific ID of supplier to retrieve
  @return a single supplier or null if supplier not found
  @throws SupplierServiceError if ID format is incorrect
*/
async function getSupplier(id) {
  if (mongoose.Types.ObjectId.isValid(id)) {
    const supplier = await Supplier.findOne({ _id: id });
    return supplier;
  }
  throw new SupplierServiceError("Invalid supplier ID format");
}

/**
  Gets a supplier based on org ID (e.g. org ID from person/org module).
  If there is no supplier found, automaticaly creates a supplier as long as the
  org ID is indeed a supplier and the requester has the proper authorization.

  @param orgId org module ID of supplier
  @param authToken Bearer <token>
  @return existing or new supplier, should never be null
  @throws AuthServiceError if unauthorized to create new supplier
  @throws SupplierServiceError for other issues
*/
async function getOrCreateSupplierByOrgId(orgId, authToken) {
  const supplier = await Supplier.findOne({orgId: orgId});

  // not found; attempt to create
  if (supplier == null) {
    const supplierToCreate = new Supplier({
      orgId: orgId,
      rating: 0,
      numReviews: 0
    });

    try {
      const newSupplier = await createSupplier(supplierToCreate, authToken);
      return newSupplier;
    }
    catch (err) {
      throw err;
    }
  }

  return supplier;
}

/**
  Gets a list of supplier-roles for the currently auth'd user.

  @param authToken Bearer <token>
  @return list of suppliers attached to user auth token
  @throws SupplierServiceError if unable to get list
*/
async function getUserSupplierOrgList(authToken) {
  try {
    const person = await idPersonByAuthToken(authToken);
    if (person == null) {
      throw new SupplierServiceError("Could not ID person via auth token when retrieving org list");
    }

    const orgRolesList = await getPersonOrgRoles(person.uuid);
    if (orgRolesList == null) {
      throw new SupplierServiceError("Could not retrieve orgs for person ID + " + person.uuid);
    }

    var orgs = [];
    for (const org of orgRolesList) {
      console.log("checking orgs...");
      var orgData = await getOrg(org.id);
      if (orgData == null) {
        throw new AuthServiceError("Unable to retrieve organization ID: " + org.id + " from org service");
      }

      if (orgData.supplier) {
        orgs.push(org);
      }
    }

    return orgs;
  }
  catch (err) {
    throw new SupplierServiceError("Could not retrieve supplier orgs list - " + err.message);
  }
}

/**
  Creates a new supplier. Org must be a supplier and caller must hold admin role in that org.

  @param supplier supplier to add
  @param authToken Bearer <token>
  @return newly created supplier
  @throws AuthServiceError if not authorized to create
  @throws SupplierServiceError on create failure
*/
async function createSupplier(supplier, authToken) {
  // authorize
  try {
    console.log("Authorizing...");

    const accessCheck = await checkSupplierOrgRoles(authToken, supplier.orgId);
  }
  catch (err) {
    console.log(err);
    throw err;
  }

  // check to make sure org ID doesn't already exist
  const supplierCheck = await Supplier.findOne({orgId: supplier.orgId});
  if (supplierCheck) {
    throw new SupplierServiceError("This supplier already exists");
  }

  console.log("Attempting to save supplier...");
  const newSupplier = await supplier.save();

  if (newSupplier) {
    return newSupplier;
  }

  throw new SupplierServiceError("Error while creating supplier");
}

/**
  Updates a supplier by ID.

  @param id resource module-specific Id of supplier to update
  @param supplier supplier data to use for update
  @param authToken Bearer <token>
  @return updated supplier info
  @throws AuthServiceError if unauthorized to update supplier
  @throws SupplierServiceError if update fails
*/
async function updateSupplier(id, supplier, authToken) {
  // first, find supplier
  if (!mongoose.Types.ObjectId.isValid(id)) {
    throw new SupplierServiceError("Invalid supplier ID format");
  }

  const supplierToUpdate = await Supplier.findById(id);
  if (supplierToUpdate == null) {
    throw new SupplierServiceError("Could not find supplier ID");
  }

  // authorize
  try {
    const accessCheck = await checkSupplierOrgRoles(authToken, supplierToUpdate.orgId);
  }
  catch (err) {
    console.log(err);
    throw err;
  }

  supplierToUpdate.orgId = suppler.orgId;
  const updatedSupplier = await supplierToUpdate.save();

  if (updatedSupplier) {
    return updatedSupplier;
  }

  throw new SupplierServiceError("Error while updating supplier");
}

/**
  Deletes a supplier by ID.

  @param id ID of supplier to delete
  @param authToken Bearer <token>
  @return deleted supplier
  @throws AuthServiceError if not authorized to delete
  @throws SupplierServiceError if delete fails
*/
async function deleteSupplier(id, authToken) {
  // find supplier
  if (!mongoose.Types.ObjectId.isValid(id)) {
    throw new SupplierServiceError("Invalid supplier ID format");
  }

  const supplierToDelete = await Supplier.findById(id);

  if (supplierToDelete == null) {
    throw new SupplierServiceError("Could not find supplier ID");
  }

  // authorize
  try {
    const accessCheck = await checkSupplierOrgRoles(authToken, supplierToDelete.orgId);
  }
  catch (err) {
    console.log(err);
    throw err;
  }

  // delete
  const res = await supplierToDelete.remove();

  return supplierToDelete;
}

/**
  Adds a review to a supplier based on supplier ID.

  @param id ID of supplier to add review to
  @param review review to add to supplier
  @param Bearer <token>
  @return newly added review
  @throws SupplierServiceError if review fails
*/
async function addReview(id, review, authToken) {
  // find supplier
  if (!mongoose.Types.ObjectId.isValid(id)) {
    throw new SupplierServiceError("Invalid supplier ID format");
  }

  const supplier = await Supplier.findById(id);

  if (supplier == null) {
    throw new SupplierServiceError("Could not find supplier ID");
  }

  // get the person ID supplying the review
  const personResponse = await idPersonByAuthToken(authToken);
  if (personResponse == null) {
    throw new SupplierServiceError("Could not retrieve person from org service via auth token");
  }
  review.personId = personResponse.uuid;
  review.name = personResponse.name;

  // add review, calc rating
  supplier.reviews.push(review);
  supplier.numReviews = supplier.reviews.length;
  supplier.rating =
    supplier.reviews.reduce((a, c) => c.rating + a, 0) /
    supplier.reviews.length;

  const updatedSupplier = await supplier.save();

  if (updatedSupplier) {
    return updatedSupplier.reviews[updatedSupplier.reviews.length - 1];
  }

  throw new SupplierServiceError("Error in adding review - could not update supplier");
}

export { getSuppliers, getSupplier, getOrCreateSupplierByOrgId, getUserSupplierOrgList, addReview, updateSupplier, deleteSupplier, createSupplier };
