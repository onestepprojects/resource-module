import Resource from '../models/resourceModel.js';
import { checkSupplierOrgRoles } from './authService.js';
import { AuthServiceError, ResourceServiceError, SupplierServiceError } from '../errors/errors.js';
import { getSupplier, createSupplier, getOrCreateSupplierByOrgId } from './supplierService.js';
import { uploadFile } from './s3service.js';
import { idPersonByAuthToken, getOrg } from '../clients/org/org-client.js';
import { issueReward } from '../clients/rewards/rewards-client.js';
import { serviceLogin } from '../clients/auth/auth-client.js';

import mongoose from 'mongoose';
import NodeGeocoder from 'node-geocoder';

/**
  Service-level methods for Resource CRUD and other functionality in the OneStep
  Relief platform Resource module. This layer sits between ExpressJS (REST)
  and Mongoose (MongoDB) and is designed to encapsulate service-level logic.
*/

// for backend service logins (rewards, for example)
const CLIENT_ID = process.env.ONESTEP_RESOURCE_CLIENT_ID;
const SECRET_KEY = process.env.ONESTEP_RESOURCE_SECRET_KEY;
const GOOGLE_API_KEY = process.env.ONESTEP_RESOURCE_GOOGLEMAPS_API_KEY;

// geocoder for extracting lat/long
const options = {
  provider: 'google',
  apiKey: GOOGLE_API_KEY
};
const geocoder = NodeGeocoder(options);

/**
  Gets a collection of resources based on (optional) filter parameters.

  @param category optional category filter
  @param supplierId optional resource module-specific supplier ID filter
  @param orgId optional org module ID filter
  @param visibility optional visibility filter (published/unpublished)
  @param searchKeyword optional search keyword filter
  @param sortOrder optional sort order
  @return list of resources based on criteria, can be empty
*/
async function getResources(category, supplierId, orgId, visibility, searchKeyword, sortOrder) {
  const resources = await Resource.find({ ...category, ...supplierId, ...orgId, ...visibility, ...searchKeyword }).sort(sortOrder);
  return resources;
}

/**
  Gets a resource based on ID. Returns null if not found.

  @param id ID of resource to retrieve
  @return resource
  @throws ResourceServiceError if ID format is invalid
*/
async function getResource(id) {
  if (mongoose.Types.ObjectId.isValid(id)) {
    const resource = await Resource.findOne({ _id: id });
    return resource;
  }
  throw new ResourceServiceError("Invalid resource ID format");
}

/**
  Creates a new resource.

  @param resource resource to add
  @param authToken Bearer token
  @return newly created resource
  @throws AuthServiceError if unauthorized to create resource
  @throws ResourceServiceError if resource creation fails
*/
async function createResource(resource, authToken) {
  try {
    // first need to ensure supplier exists; auto creates if not
    const supplier = await getOrCreateSupplierByOrgId(resource.orgId, authToken);

    // authorize
    const accessCheck = await checkSupplierOrgRoles(authToken, supplier.orgId);

    // geocode and set lat/long
    const locStr = (resource.location.address || '') + ' ' + (resource.location.city || '') +
                   (resource.location.postalCode || '') + ' ' + (resource.location.country || '');
    try {
      const locRes = await geocoder.geocode(locStr);
      resource.location.lat = locRes[0].latitude;
      resource.location.long = locRes[0].longitude;
    }
    catch (err) {
      // unable to retrieve from API; log error
      console.log("Unable to set lat/long: " + err.message);
    }

    // get person ID to set creator and set supplier ID
    const personResponse = await idPersonByAuthToken(authToken);
    if (personResponse == null) {
      throw new ResourceServiceError("Could not retrieve person from org service via auth token");
    }
    resource.creatorId = personResponse.uuid;
    resource.supplierId = supplier._id;

    const newResource = await resource.save();
    if (newResource) {
      // attempt to issue reward
      const serviceAuthToken = await serviceLogin(CLIENT_ID, SECRET_KEY);
      if (serviceAuthToken == null) {
        console.log("Cannot issue reward -- Failed to retrieve auth token for resource service");
      }
      else {
        console.log("got auth token: " + serviceAuthToken.access_token);
        const reward = await issueReward("Created resource: " + newResource.name + " (ID: " + newResource._id + ")", 100, newResource.creatorId, serviceAuthToken.access_token);
        if (reward == null) {
          // reward failed. we proceed to create resource but log the failure.
          console.log("Unable to issue reward for creating resource ID: " + newResource._id);
        }
      }

      return newResource;
    }

    throw new ResourceServiceError("Error in creating resource");
  }
  catch (err) {
    console.log(err);
    throw err;
  }
}

/**
  Updates a resource by ID.

  @param id ID of resource to update
  @param resource resource details for update
  @param authToken Bearer token
  @return updated resource
  @throws AuthServiceError if unauthorized to update resource
  @throws SupplierServiceError if supplier tied to resource not found
  @throws ResourceServiceError if update fails
*/
async function updateResource(id, resource, authToken) {
  try {
    // find resource
    if (!mongoose.Types.ObjectId.isValid(id)) {
      throw new ResourceServiceError("Invalid resource ID format");
    }

    const resourceToUpdate = await Resource.findById(id);
    if (resourceToUpdate == null) {
      throw new ResourceServiceError("Could not find resource ID");
    }

    // first need to ensure supplier exists
    const supplier = await getSupplier(resourceToUpdate.supplierId);
    if (supplier == null) {
      throw new SupplierServiceError("Could not find supplier ID specified in existing resource");
    }

    // authorize
    const accessCheck = await checkSupplierOrgRoles(authToken, supplier.orgId);

    // if trying to publish a resource, ensure this resource's supplier has payment information.
    if (resourceToUpdate.visibility === "UNPUBLISHED" && resource.visibility === "PUBLISHED") {
      const org = await getOrg(supplier.orgId);
      if (org == null || (org.paymentAccount.payID == null && org.paymentAccount.blockchainAddress == null)) {
        throw new SupplierServiceError("Cannot publish resource - please ensure this supplier has set up payment information");
      }
    }

    // if location changed, recalc lat/long
    if (JSON.stringify(resource.location) !== JSON.stringify(resourceToUpdate.location)) {
      const locStr = (resource.location.address || '') + ' ' + (resource.location.city || '') +
                     (resource.location.postalCode || '') + ' ' + (resource.location.country || '');
      try {
        const locRes = await geocoder.geocode(locStr);
        resource.location.lat = locRes[0].latitude;
        resource.location.long = locRes[0].longitude;
      }
      catch (err) {
        // unable to retrieve from API; log error
        console.log("Unable to set lat/long: " + err.message);
      }
    }

    resourceToUpdate.name = resource.name;
    resourceToUpdate.description = resource.description;
    resourceToUpdate.category = resource.category;
    resourceToUpdate.price = resource.price;
    resourceToUpdate.countInStock = resource.countInStock;
    resourceToUpdate.location = resource.location;
    resourceToUpdate.visibility = resource.visibility;
    resourceToUpdate.shippingOption = resource.shippingOption;
    resourceToUpdate.productCode = resource.productCode;
    resourceToUpdate.image = resource.image;
    resourceToUpdate.brand = resource.brand;
    resourceToUpdate.note = resource.note;
    resourceToUpdate.condition = resource.condition;

    const updatedResource = await resourceToUpdate.save();

    if (updatedResource) {
      return updatedResource;
    }

    throw new ResourceServiceError("Error in updating resource");
  }
  catch (err) {
    console.log(err);
    throw err;
  }
}

/**
  Deletes a resource by ID.

  @param id ID of resource to delete
  @param authToken Bearer token
  @return deleted resource
  @throws SupplierServiceError if unable to find supplier for resource
  @throws AuthServiceError if not authorized to delete resource
  @throws ResourceServiceError if delete fails
*/
async function deleteResource(id, authToken) {
  try {
    // find resource
    if (!mongoose.Types.ObjectId.isValid(id)) {
      throw new ResourceServiceError("Invalid resource ID format");
    }

    const resourceToDelete = await Resource.findById(id);

    if (resourceToDelete == null) {
      throw new ResourceServiceError("Could not find resource ID");
    }

    // first need to ensure supplier exists
    const supplier = await getSupplier(resourceToDelete.supplierId);
    if (supplier == null) {
      throw new SupplierServiceError("Could not find supplier ID specified in resource");
    }

    // authorize
    const accessCheck = await checkSupplierOrgRoles(authToken, supplier.orgId);

    // delete
    const res = await resourceToDelete.remove();

    return resourceToDelete;
  }
  catch (err) {
    console.log(err);
    throw err;
  }

}

/**
  Adds a review to a resource based on resource ID.

  @param id ID of resource to add review to
  @param review review to add to resource
  @param authToken Bearer token
  @return newly added review
  @throws ResourceServiceError if add review fails
*/
async function addReview(id, review, authToken) {
  // find resource
  if (!mongoose.Types.ObjectId.isValid(id)) {
    throw new ResourceServiceError("Invalid resource ID format");
  }

  const resource = await Resource.findById(id);

  if (resource == null) {
    throw new ResourceServiceError("Could not find resource ID");
  }

  // get the person ID supplying the review
  const personResponse = await idPersonByAuthToken(authToken);
  if (personResponse == null) {
    throw new ResourceServiceError("Could not retrieve person from org service via auth token");
  }
  review.personId = personResponse.uuid;
  review.name = personResponse.name;

  // add review, calc rating
  resource.reviews.push(review);
  resource.numReviews = resource.reviews.length;
  resource.rating =
    resource.reviews.reduce((a, c) => c.rating + a, 0) /
    resource.reviews.length;


  const updatedResource = await resource.save();

  if (updatedResource) {
    return updatedResource.reviews[updatedResource.reviews.length - 1];
  }

  throw new ResourceServiceError("Error in adding review - could not update resource");
}

/**
  Adds to resource view count based on ID.

  @param id ID of resource for which to increment view count
  @return updated resource (w/ updated view count)
  @throws ResourceServiceError if update fails
*/
async function incrementResourceViews(id) {
  // find resource
  if (!mongoose.Types.ObjectId.isValid(id)) {
    throw new ResourceServiceError("Invalid resource ID format");
  }

  const resource = await Resource.findOne({ _id: id });

  if (resource == null) {
    throw new ResourceServiceError("Could not find resource ID");
  }

  resource.views++;

  const updatedResource = await resource.save();

  if (updatedResource) {
    return updatedResource;
  }

  throw new ResourceServiceError("Error in incrementing resource views");
}

/**
  Uploads an image for a resource (via resource ID).

  @param id resource ID
  @param fileData base64 encoded file data
  @param authToken Bearer token
  @return updated resource
  @throws ResourceServiceError on error
*/
async function uploadResourceImage(id, fileData, authToken) {
  // find resource
  if (!mongoose.Types.ObjectId.isValid(id)) {
    throw new ResourceServiceError("Invalid resource ID format");
  }

  const resource = await Resource.findOne({ _id: id });

  if (resource == null) {
    throw new ResourceServiceError("Could not find resource ID");
  }

  // check supplier
  const supplier = await getSupplier(resource.supplierId);
  if (supplier == null) {
    throw new SupplierServiceError("Could not find supplier ID specified in resource");
  }

  // authorize
  const accessCheck = await checkSupplierOrgRoles(authToken, supplier.orgId);

  // upload, set resource image
  try {
    const url = await uploadFile(id, fileData);
    if (url) {
      console.log("Image uploaded with URL: " + url);

      resource.image = url;
      const updatedResource = await resource.save();
      if (updatedResource) {
        return updatedResource;
      }
    }
  }
  catch (err) {
    throw new ResourceServiceError("S3 upload failure: " + err.message);
  }

  throw new ResourceServiceError("Resource service failed to upload image");
}

export { getResources, getResource, incrementResourceViews, addReview, updateResource, deleteResource, createResource, uploadResourceImage };
