import { getAuthUser } from '../clients/auth/auth-client.js';
import { idPersonByAuthToken, getPersonOrgRoles, getOrg } from '../clients/org/org-client.js';
import { AuthServiceError } from '../errors/errors.js';

/**
  Provides authorization services for the OneStep Resource module.
*/

const REQ_ROLES = [ 'ADMIN', 'GLOBAL_ADMIN' ];

/**
  Checks that person holds a GLOBAL_ADMIN role in some org.

  @param authToken Bearer token
  @return true if holds the role
  @throws AuthServiceError if org service fails or person is not a global admin
*/
export async function checkGlobalAdmin(authToken) {
  // retrieve person
  const person = await idPersonByAuthToken(authToken);
  if (person == null) {
    throw new AuthServiceError("Unable to retrieve person from org service");
  }

  // retrieve person org roles
  const orgRoles = await getPersonOrgRoles(person.uuid);
  if (orgRoles == null) {
    throw new AuthServiceError("Unable to retrieve person's org roles from org service");
  }

  // check for global admin role
  for (const org of orgRoles) {
    if (org.roles.includes("GLOBAL_ADMIN")) {
      return true;
    }
  }

  throw new AuthServiceError("This person is not a GLOBAL_ADMIN");
}

/**
  Checks supplier privileges
  (e.g. that authToken bearer holds admin role in an org labeled supplier that matches supplied orgId)

  @param authToken Bearer token
  @param orgId org ID to check
  @return true if holds proper roles
  @throws AuthServiceError if org service fails, or not authorized
*/
export async function checkSupplierOrgRoles(authToken, orgId) {
  // retrieve person
  const person = await idPersonByAuthToken(authToken);
  if (person == null) {
    throw new AuthServiceError("Unable to retrieve person from org service");
  }

  console.log("Retrieved person ID: " + person.uuid);

  // retrieve person org roles
  const orgRoles = await getPersonOrgRoles(person.uuid);
  if (orgRoles == null) {
    throw new AuthServiceError("Unable to retrieve person's org roles from org service");
  }

  console.log("Retrieved org roles: " + orgRoles);

  // find the org ID in question and ensure they are a supplier and hold a required role
  for (const org of orgRoles) {
    if (org.id === orgId) {

      // get full org data. can omit once team 1 API updates their orgRoles call
      const orgData = await getOrg(org.id);
      if (orgData == null) {
        throw new AuthServiceError("Unable to retrieve organization ID: " + org.id + " from org service");
      }

      // is org a supplier?
      if (orgData.supplier == false) {
        throw new AuthServiceError("This organization is not flagged as a supplier");
      }

      // does person hold proper roles in org?
      for (const role of REQ_ROLES) {
        if (org.roles.includes(role)) {
          return true;
        }
      }

    }
  }

  throw new AuthServiceError("This person does not hold a required role in the given organization");
}
