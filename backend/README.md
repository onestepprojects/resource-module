# OneStep Resource Module Backend

## How to Run
The backend is written in NodeJS, using ExpressJS for REST, a service layer
and Mongoose to at the bottom to interface with MongoDB. In the module/backend
directory, run npm install followed by npm start in your CLI/IDE to start the backend.

***TODO: This will be running from a docker image; update this with relevant instructions***

## REST API

***TODO: REST paths are likely to change (cosmetically) to make semantically consistent with other OneStep REST calls***

### Get All/Many Resources
GET .../api/resources \
*Attributes*: \
(optional query parameters):
- ?category= a resource category
- ?supplierId= a supplier ID (orgId)
- ?searchKeyword= a keyword on which to search resources
- ?sortOrder= sort by price, 'lowest' puts lowest first
<br><br>
*Returns*: \
json with a list of all resources that match the query criteria. Example:
```json

```

### Get Resource
GET .../api/resources/{resource-id} \
*Attributes*: \
resource-id (String): ID of resource to retrieve
<br><br>
*Returns*: \
json representation of the chosen resource or 404 if not found. Example:
```json

```

### Create Resource
POST .../api/resources/
*Attributes*: \
JSON object, example:
```json
{
  "creatorId": "default_personId",
  "supplierId": "supplierId",  
  "name": "name",
  "description": "description",
  "category": "category",
  "price": 5.00,
  "countInStock": 10,
  "location": {
    "lat": 0,
    "long": 0,
    "address": "street address",
    "city": "city",
    "postalCode": "02155",
    "country": "USA"
  },
  "shippingOption": "<enum> PICKUP <or> DELIVERY",
  "productCode": "productCode",
  "image": "www.imageurl.com/image_name.jpg",
  "brand": "brand",
  "note": "note",
  "condition": "<enum> NEW <or> USED",
}
```
<br><br>
*Returns*: \
json representation of the new resource or 500 if not successful. Example:
```json

```

### Update Resource
PUT .../api/resources/
*Attributes*: \
JSON object, example:
```json
{
  "creatorId": "default_personId",
  "supplierId": "supplierId",  
  "name": "name",
  "description": "description",
  "category": "category",
  "price": 5.00,
  "countInStock": 10,
  "location": {
    "lat": 0,
    "long": 0,
    "address": "street address",
    "city": "city",
    "postalCode": "02155",
    "country": "USA"
  },
  "shippingOption": "<enum> PICKUP <or> DELIVERY",
  "productCode": "productCode",
  "image": "www.imageurl.com/image_name.jpg",
  "brand": "brand",
  "note": "note",
  "condition": "<enum> NEW <or> USED",
}
```
<br><br>
*Returns*: \
json representation of the updated resource or 500 if not successful. Example:
```json

```

### Delete Resource
DELETE .../api/resources/{resource-id}
*Attributes*: \
resource-id (String): ID of resource to delete
<br><br>
*Returns*: \
***TODO: update***
200 OK or 404 if not found.

### Add a Resource Review
POST .../api/resources/{resource-id}/reviews
*Attributes*: \
JSON object, example:
```json
{
  "personid": "default_personid",
  "rating": 3,
  "comment": "comment"
}
```
<br><br>
*Returns*: \
json representation of the new review, 500 if not successful, or 404 if resource ID not found. Example:
```json

```

### Add a Resource Review
POST .../api/resources/{resource-id}/reviews
*Attributes*: \
JSON object, example:
```json
{
  "personid": "default_personid",
  "rating": 3,
  "comment": "comment"
}
```
<br><br>
*Returns*: \
json representation of the new review, 500 if not successful, or 404 if resource ID not found. Example:
```json

```

### Increment Resource Views
GET ...api/resources/increment-views/:id
*Attributes*: \
id (String): Resource ID for which to increment pageviews.
*Returns*: \
200 on success or 404 if ID not found.

### Get All Suppliers
GET .../api/suppliers \
*Attributes*: \
None
<br><br>
*Returns*: \
json with a list of all suppliers. Example:
```json
[
  {
      "rating": 0,
      "numReviews": 0,
      "_id": "606742ad0ff5e738f837b96e",
      "orgId": "Home Depot",
      "blockchainAccount": "XXXXXXXXXXXXXXXXXXXXXXXXXXXX",
      "supplierPreferences": {
        "_id": "606742ad0ff5e738f837b96f",
        "location": {
          "_id": "606742ad0ff5e738f837b970",
          "lat": 48,
          "long": -49,
          "address": "1 Home Depot Street",
          "city": "Home Depot Town",
          "postalCode": "01234",
          "country": "USA"
        },
        "shippingOption": "DELIVERY"
      },
      "reviews": [],
      "__v": 0
    }
]
```

### Get Supplier by Internal ID
GET .../api/suppliers/{supplier-id} \
*Attributes*: \
supplier-id (String): resource module-specific ID of supplier to retrieve
<br><br>
*Returns*: \
json representation of the chosen supplier or 404 if not found. Example:
```json
{
  "rating": 0,
  "numReviews": 0,
  "_id": "606742ad0ff5e738f837b96e",
  "orgId": "Home Depot",
  "blockchainAccount": "XXXXXXXXXXXXXXXXXXXXXXXXXXXX",
  "supplierPreferences": {
    "_id": "606742ad0ff5e738f837b96f",
    "location": {
      "_id": "606742ad0ff5e738f837b970",
      "lat": 48,
      "long": -49,
      "address": "1 Home Depot Street",
      "city": "Home Depot Town",
      "postalCode": "01234",
      "country": "USA"
    },
    "shippingOption": "DELIVERY"
  },
  "reviews": [],
  "__v": 0
}
```

### Get Supplier by Organization ID
GET .../api/suppliers/orgid/{org-id} \
*Attributes*: \
org-id (String): Person/org module organization ID of supplier to retrieve
<br><br>
*Returns*: \
json representation of the chosen supplier or 404 if not found. Example:
```json
{
  "rating": 0,
  "numReviews": 0,
  "_id": "606742ad0ff5e738f837b96e",
  "orgId": "Home Depot",
  "blockchainAccount": "XXXXXXXXXXXXXXXXXXXXXXXXXXXX",
  "supplierPreferences": {
    "_id": "606742ad0ff5e738f837b96f",
    "location": {
      "_id": "606742ad0ff5e738f837b970",
      "lat": 48,
      "long": -49,
      "address": "1 Home Depot Street",
      "city": "Home Depot Town",
      "postalCode": "01234",
      "country": "USA"
    },
    "shippingOption": "DELIVERY"
  },
  "reviews": [],
  "__v": 0
}
```

### Create Supplier
POST .../api/suppliers/
*Attributes*: \
JSON object, example:
```json
{
  "orgId": "Home Depot",
  "supplierPreferences": {
    "location": {
		  "lat": 48,
			"long": -49,
			"address": "1 Home Depot Street",
			"city": "Home Depot Town",
			"postalCode": "01234",
			"country": "USA"
	  },
    "shippingOption": "DELIVERY"
	}
}
```
<br><br>
*Returns*: \
json representation of the new supplier or 500 if not successful. Example:
```json

```

### Update Supplier
PUT .../api/suppliers/{supplier-id}
*Attributes*: \
JSON object, example:
```json
{
  "orgId": "Home Depot",
  "supplierPreferences": {
    "location": {
		  "lat": 48,
			"long": -49,
			"address": "1 Home Depot Street",
			"city": "Home Depot Town",
			"postalCode": "01234",
			"country": "USA"
	  },
    "shippingOption": "DELIVERY"
	}
}
```
<br><br>
*Returns*: \
json representation of the updated supplier or 500 if not successful. Example:
```json

```

### Delete Supplier
DELETE .../api/suppliers/{supplier-id}
*Attributes*: \
supplier-id (String): ID of supplier to delete
<br><br>
*Returns*: \
***TODO: update***
200 OK or 404 if not found.

### Add a Supplier Review
POST .../api/suppliers/{supplier-id}/reviews
*Attributes*: \
JSON object, example:
```json
{
  "personid": "default_personid",
  "rating": 3,
  "comment": "comment"
}
```
<br><br>
*Returns*: \
json representation of the new review, 500 if not successful, or 404 if resource ID not found. Example:
```json

```
