// if running locally, the following is important to load .env, and this config file must be first import in server.js
import dotenv from "dotenv";
dotenv.config();

export default {
  PORT: process.env.PORT || 5000,
  MONGODB_URL: process.env.ONESTEP_RESOURCE_MONGODB_URL
  // MONGODB_URL: process.env.MONGODB_URL || 'mongodb://localhost/resourcelocaldb'
};
