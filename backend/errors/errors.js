/**
  Error types for Resource module. As of 4/30/2021, just stubs to assist in returning error codes from API.
*/

export class SupplierServiceError extends Error {
  constructor(...params) {
    super(...params);
  }
}

export class ResourceServiceError extends Error {
  constructor(...params) {
    super(...params);
  }
}

export class OrderServiceError extends Error {
  constructor(...params) {
    super(...params);
  }
}

export class AuthServiceError extends Error {
  constructor(...params) {
    super(...params);
  }
}
