import express from 'express';
import Supplier from '../models/supplierModel.js';
import { isAuth } from './auth.js';
import { AuthServiceError } from '../errors/errors.js';

/**
  REST paths for Supplier CRUD and other functionality in the OneStep
  Relief platform Resource module.
*/

// supplier service
import { getSuppliers, getSupplier, getOrCreateSupplierByOrgId, getUserSupplierOrgList, addReview, updateSupplier, deleteSupplier, createSupplier } from '../services/supplierService.js';

const router = express.Router();

/**
  Gets all suppliers.
*/
router.get('/', isAuth, async (req, res) => {
  const suppliers = await getSuppliers();
  res.send(suppliers);
});


/**
  Gets a supplier by internal (resource module assigned) ID or returns error.
*/
router.get('/:id', isAuth, async (req, res) => {
  try {
    const supplier = await getSupplier(req.params.id);

    if (supplier) {
      res.send(supplier);
    } else {
      res.status(404).send({ message: 'Supplier Not Found.' });
    }
  }
  catch (err) {
    console.log(err);
    res.status(500).send({ message: 'Invalid supplier ID format' });
  }
});

/**
  Gets a supplier by org ID (e.g. ID from team 1 module).
  If the supplier doesn't exist, the backend automatically generates the supplier.
  If the org ID is invalid (e.g. not a supplier, or the requester doesn't have the proper role)
  then returns 401.
*/
router.get('/orgid/:id', isAuth, async (req, res) => {
  const authToken = req.header('Authorization');

  try {
    const supplier = await getOrCreateSupplierByOrgId(req.params.id, authToken);
    res.send(supplier);
  }
  catch (err) {
    if (err instanceof AuthServiceError) {
      res.status(401).send({ message: 'Authorization error while retrieving/creating supplier by Org ID - ' + err.message });
    }
    else {
      res.status(500).send({ message: 'Could not retrieve and/or automatically create supplier - ' + err.message});
    }
  }
});

/**
  Gets all supplier orgs/roles associated with the authenticated user
*/
router.get('/user/supplier-org-list', isAuth, async (req, res) => {
  const authToken = req.header('Authorization');

  try {
    const orgList = await getUserSupplierOrgList(authToken);
    res.send(orgList);
  }
  catch (err) {
    res.status(401).send({ message: 'Authorization error while retrieving user supplier orgs - ' + err.message });
  }
});


/**
  Creates a new supplier.
*/
router.post('/', isAuth, async (req, res) => {
  const authToken = req.header('Authorization');

  const supplier = new Supplier({
    orgId: req.body.orgId,
    rating: 0,
    numReviews: 0
  });

  try {
    console.log("Attempting to create supplier...");

    const newSupplier = await createSupplier(supplier, authToken);
    return res.status(201).send(newSupplier);
  }
  catch (err) {
    console.log(err);
    if (err instanceof AuthServiceError) {
      return res.status(401).send({ message: "Could not create supplier: " + err.message });
    }
    else {
      return res.status(500).send({ message: err.message });
    }
  }
});

/**
  Updates a supplier based on ID.
*/
router.put('/:id', isAuth, async (req, res) => {
  const authToken = req.header('Authorization');

  const supplier = new Supplier({
    orgId: this.req.orgId
  });

  try {
    const updatedSupplier = await updateSupplier(req.params.id, supplier, authToken);
    return res.send(updatedSupplier);
  }
  catch (err) {
    if (err instanceof AuthServiceError) {
      return res.status(401).send({message: "Could not update supplier: " + err.message});
    }
    else {
      return res.status(404).send({message: "Could not update supplier: " + err.message});
    }
  }
});

/**
  Deletes a supplier based on ID.
*/
router.delete('/:id', isAuth, async (req, res) => {
  const authToken = req.header('Authorization');

  try {
    const deletedSupplier = await deleteSupplier(req.params.id, authToken);
    return res.send(deletedSupplier);
  }
  catch (err) {
    if (err instanceof AuthServiceError) {
      return res.status(401).send({message: "Could not delete supplier: " + err.message});
    }
    else {
      return res.status(404).send({message: "Could not delete supplier: " + err.message});
    }
  }
});

/**
  Posts a new review for a supplier.
*/
router.post('/:id/reviews', isAuth, async (req, res) => {
  const authToken = req.header('Authorization');

  const review = {
    rating: Number(req.body.rating),
    comment: req.body.comment,
  }

  try {
    const result = await addReview(req.params.id, review, authToken);
    res.send(result);
  }
  catch (err) {
    return res.status(404).send({message: "Could not add review to supplier: " + err.message});
  }
});

export default router;
