import { getAuthUser } from '../clients/auth/auth-client.js';

/**
    Authentication for the Resource module routes. Access to module API requires a valid auth token.
    ExpressJS allows handling of this via middleware function below.
*/

/**
    Simple check that confirms that a user is logged into the OneStep platform.
*/
const isAuth = async (req, res, next) => {
  console.log("Verifying auth token");
  
  if (req.header('Authorization') == undefined) {
    res.status(401).send({ message: 'No auth token supplied in request' });
    return;
  }

  const authToken = req.header('Authorization');

  // authenticate
  const userRes = await getAuthUser(authToken);
  if (userRes == null) {
    res.status(401).send({ message: 'Invalid auth token' });
    return;
  }

  return next();
}

export { isAuth };
