import express from 'express';
import OrderItem from '../models/orderItemModel.js';
import { isAuth } from './auth.js';
import { AuthServiceError, SupplierServiceError, ResourceServiceError, OrderServiceError } from '../errors/errors.js';

/**
  REST paths for Order CRUD and other functionality in the OneStep
  Relief platform Resource module.
*/

// order service
import { getOrderItems, getOrderItem, createOrderItem, updateOrderItemState, deleteOrderItem, payOrderItem } from '../services/orderService.js';

const router = express.Router();

/**
  Gets all orders for a given supplier ID and/or person ID in the desired order.
  Query params can include supplierId, orgId, personId, searchKeyword and sortOrder.
*/
router.get('/', isAuth, async (req, res) => {
  // parse request parameters
  const supplierId = req.query.supplierId ? { supplierId : req.query.supplierId } : {};
  const orgId = req.query.orgId ? { orgId : req.query.orgId } : {};
  const personId = req.query.personId ? { personId : req.query.personId } : {};
  const searchKeyword = req.query.searchKeyword
    ? {
        name: {
          $regex: req.query.searchKeyword,
          $options: 'i',
        },
      }
    : {};
  const sortOrder = req.query.sortOrder
    ? req.query.sortOrder === 'lowest'
    : { _id: -1 };


  const orderItems = await getOrderItems(supplierId, orgId, personId, searchKeyword, sortOrder);
  res.send(orderItems);
});


/**
  Gets an Order by ID or returns 404.
  Query param must be desired order ID.
*/
router.get('/:id', isAuth, async (req, res) => {
  try {
    const order = await getOrderItem(req.params.id);

    if (order) {
      res.send(order);
    } else {
      res.status(404).send({ message: 'Order Not Found.' });
    }
  }
  catch (err) {
    console.log(err);
    res.status(500).send({ message: 'Invalid order ID format' });
  }
});

/**
  Creates a new Order. For now, Orders consist of a single OrderItem.
  Returns new order on success.
*/
router.post('/', isAuth, async (req, res) => {
  const authToken = req.header('Authorization');

  const orderItem = new OrderItem({
    resource: req.body.resource,
    qty: req.body.qty,
    shippingOption: req.body.shippingOption,
    shipToLoc: req.body.shipToLoc,
  });

  try {
    const newOrderItem = await createOrderItem(orderItem, authToken);
    res.status(201).send(newOrderItem);
  }
  catch (err) {
    if (err instanceof ResourceServiceError) {
      res.status(404).send({ message: "Could not create order: " + err.message });
    }
    else {
      res.status(500).send({ message: "Could not create order: " + err.message });
    }
  }
});


/**
  Update an order state by ID.
*/
router.put('/:id', isAuth, async (req, res) => {
  const authToken = req.header('Authorization');

  const newOrderState = req.body.orderState;

  try {
    const updatedOrder = await updateOrderItemState(req.params.id, newOrderState, authToken);
    res.send(updatedOrder);
  }
  catch (err) {
    if (err instanceof AuthServiceError) {
      res.status(401).send({ message: "Could not update order state: " + err.message });
    }
    else {
      res.status(500).send({ message: "Could not update order state: " + err.message });
    }
  }
});

/**
  Deletes an Order based on ID. Returns details of deleted item on success.
*/
router.delete('/:id', isAuth, async (req, res) => {
  const authToken = req.header('Authorization');

  try {
    const deletedOrderItem = await deleteOrderItem(req.params.id, authToken);
    res.send(deletedOrderItem);
  }
  catch (err) {
    res.status(401).send({ message: "Could not delete order item: " + err.message });
  }
});

/**
  Pays an order.
*/
router.post('/pay', isAuth, async (req, res) => {
  const authToken = req.header('Authorization');
  const sender = { payId: req.body.payId };

  try {
    console.log(req.body.orderId);
    const tx = await payOrderItem(req.body.orderId, sender, authToken);
    res.send(tx);
  }
  catch (err) {
    if (err instanceof AuthServiceError) {
      return res.status(401).send({ message: "Could not pay order: " + err.message });
    }
    else {
      res.status(500).send({ message: err.message });
    }
  }
})

export default router;
