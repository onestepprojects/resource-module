import express from 'express';
import Resource from '../models/resourceModel.js';
import { isAuth } from './auth.js';
import { AuthServiceError, SupplierServiceError } from '../errors/errors.js';

/**
  REST paths for Resource CRUD and other functionality in the OneStep
  Relief platform Resource module.
*/

// resource service
import { getResources, getResource, incrementResourceViews, addReview, updateResource, deleteResource, createResource, uploadResourceImage } from '../services/resourceService.js';

const router = express.Router();

/**
  Gets resouces based on certain parameters such as category, search,
  and/or supplierId, with a given sort approach by calling service.
*/
router.get('/', isAuth, async (req, res) => {
  console.log('GET Resources route called');

  const category = req.query.category ? { category: req.query.category } : {};
  const supplierId = req.query.supplierId ? { supplierId: req.query.supplierId } : {};
  const orgId = req.query.orgId ? { orgId: req.query.orgId } : {};
  const visibility = req.query.visibility  ? { visibility : req.query.visibility  } : {};
  const searchKeyword = req.query.searchKeyword
    ? {
        name: {
          $regex: req.query.searchKeyword,
          $options: 'i',
        },
      }
    : {};
  const sortOrder = req.query.sortOrder
    ? req.query.sortOrder === 'lowest'
      ? { price: 1 }
      : { price: -1 }
    : { _id: -1 };

  const resources = await getResources(category, supplierId, orgId, visibility, searchKeyword, sortOrder);

  console.log('Sending response...');
  res.send(resources);
});

/**
  Gets a resource by ID or returns 404.
*/
router.get('/:id', isAuth, async (req, res) => {
  try {
    const resource = await getResource(req.params.id);

    if (resource) {
      res.send(resource);
    } else {
      res.status(404).send({ message: 'Resource Not Found.' });
    }
  }
  catch (err) {
    console.log(err);
    res.status(500).send({ message: 'Invalid resource ID format' });
  }
});

/**
  Creates a new resource.
*/
router.post('/', isAuth, async (req, res) => {
  const authToken = req.header('Authorization');

  try {
    const resource = new Resource({
      orgId: req.body.orgId,
      name: req.body.name,
      description: req.body.description,
      category: req.body.category,
      price: req.body.price,
      countInStock: req.body.countInStock,
      rating: 0,
      numReviews: 0,
      views: 0,
      location: req.body.location,
      shippingOption: req.body.shippingOption,

      // optional fields
      productCode: req.body.productCode,
      image: req.body.image,
      brand: req.body.brand,
      note: req.body.note,
      condition: req.body.condition,
    });

    const newResource = await createResource(resource, authToken);
    res.status(201).send(newResource);
  }
  catch (err) {
    if (err instanceof AuthServiceError) {
      res.status(401).send({ message: "Could not create resource: " + err.message });
    }
    else if (err instanceof SupplierServiceError) {
      res.status(404).send({ message: "Could not create resource: " + err.message });
    }
    else {
      res.status(500).send({ message: "Could not create resource: " + err.message });
    }
  }
});

/**
  Updates a resource based on ID.
*/
router.put('/:id', isAuth, async (req, res) => {
  const authToken = req.header('Authorization');

  try {
    console.log("Attempting to set price to : " + req.body.price);
    
    const resource = new Resource({
      name: req.body.name,
      description: req.body.description,
      category: req.body.category,
      price: req.body.price,
      countInStock: req.body.countInStock,
      location: req.body.location,
      visibility: req.body.visibility,
      shippingOption: req.body.shippingOption,
      productCode: req.body.productCode,
      image: req.body.image,
      brand: req.body.brand,
      note: req.body.note,
      condition: req.body.condition,
    });

    const updatedResource = await updateResource(req.params.id, resource, authToken);
    return res.send(updatedResource);
  }
  catch (err) {
    if (err instanceof AuthServiceError) {
      return res.status(401).send({message: "Could not update resource: " + err.message});
    }
    else if (err instanceof SupplierServiceError) {
      return res.status(404).send({message: "Could not update resource: " + err.message});
    }
    else {
      return res.status(404).send({message: "Could not update resource: " + err.message});
    }
  }

});

/**
  Deletes a resource based on ID.
*/
router.delete('/:id', isAuth, async (req, res) => {
  const authToken = req.header('Authorization');

  try {
    const deletedResource = await deleteResource(req.params.id, authToken);
    return res.send(deletedResource);
  }
  catch (err) {
    if (err instanceof AuthServiceError) {
      return res.status(401).send({message: "Could not delete resource: " + err.message});
    }
    else if (err instanceof SupplierServiceError) {
      return res.status(404).send({message: "Could not delete resource: " + err.message});
    }
    else {
      return res.status(404).send({message: "Could not delete resource: " + err.message});
    }
  }
});

/**
  Posts a new review for a resource.
*/
router.post('/reviews/:id', isAuth, async (req, res) => {
  const authToken = req.header('Authorization');

  const review = {
    rating: Number(req.body.rating),
    comment: req.body.comment,
  }

  try {
    const result = await addReview(req.params.id, review, authToken);
    res.send(result);
  }
  catch (err) {
    return res.status(404).send({message: "Could not add review to resource: " + err.message});
  }
});

/**
  Increments resource views.
*/
router.get('/increment-views/:id', isAuth, async (req, res) => {
  try {
    const resource = await incrementResourceViews(req.params.id);
    res.send(resource);
  }
  catch (err) {
    res.status(404).send({ message: "Resource not found" });
  }
});

/**
  Uploads an image for a resource.
*/
router.put('/image/:id', isAuth, async (req, res) => {
  const authToken = req.header('Authorization');

  try {
    // construct image object data
    let fileData = {
      name: req.body.name,
      type: req.body.type,
      data: req.body.data
    }

    console.log("Uploading image: " + fileData.name + " for resource ID: " + req.params.id);

    if (fileData.type != "image/jpeg" && fileData.type != "image/png"){
      return res.status(400).send({ message: "Image uploads must be JPG or PNG" });
    }

    const resource = await uploadResourceImage(req.params.id, fileData, authToken);

    console.log("Finished upload attempt");

    return res.send(resource);
  }
  catch (err) {
    return res.status(500).send({ message: err.message });
  }
});

export default router;
