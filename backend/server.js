import config from './config.js';
import require from 'requirejs';
import express from 'express';
import path from 'path';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import resourceRoute from './routes/resourceRoute.js';
import supplierRoute from './routes/supplierRoute.js';
import orderRoute from './routes/orderRoute.js';

import { dirname } from 'path';
import { fileURLToPath } from 'url';

const __dirname = dirname(fileURLToPath(import.meta.url));

const cors = require('cors');

const mongodbUrl = config.MONGODB_URL;
mongoose
  .connect(mongodbUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  })
  .catch((error) => console.log(error.reason));

const app = express();
// app.use(bodyParser.json());
app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({limit: '50mb'}));
app.use(cors());
app.use('/api/resources', resourceRoute);
app.use('/api/suppliers', supplierRoute);
app.use('/api/orders', orderRoute);

app.listen(config.PORT, () => {
  console.log('Server started at http://localhost:5000');
});
