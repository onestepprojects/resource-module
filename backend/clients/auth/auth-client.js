import axios from 'axios';

/**
  Client to access OneStep authorization REST API.
*/
const URL = process.env.ONESTEP_AUTHORIZATION_URL;
const USER_PATH = '/user';
const SERVICE_LOGIN_PATH = '/servicelogin';

/**
  Gets user details from auth service. Rejected if auth token invalid/expired.

  @param authToken authentication token for which to get user
  @return response from auth API containing user data if auth token is valid, otherwise logs error and null
*/
export async function getAuthUser(authToken) {
  let header = { headers: { "Authorization": authToken } };

  try {
    console.log("GET: " + URL + USER_PATH);
    const response = await axios.get(URL + USER_PATH, header);
    return response.data;
  }
  catch (error) {
    // console.error(error);
    return null;
  }
}

/**
   * Calls REST API from auth service to retrieve access token for a service.
   * Converts clientId:secretKey to base64 string and passes in header.
   * Auth token is stored in access_token field.
   *
   * @param clientId client ID for service
   * @param secretKey secret key for client
   * @return response from auth API containing access token for service, otherwise logs error and null
*/
export async function serviceLogin(clientId, secretKey) {
  // encode to base64 and set header
  let unencoded = clientId + ":" + secretKey;
  let encoded = Buffer.from(unencoded).toString('base64');
  console.log("attempting service login with base64: " + encoded);
  let header = { headers: { "Authorization": "Basic " + encoded } };

  try {
    const response = await axios.post(URL + SERVICE_LOGIN_PATH, null, header);
    return response.data;
  }
  catch (error) {
    console.error(error);
    return null;
  }
}
