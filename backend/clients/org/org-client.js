import axios from 'axios';

/**
  Client to access OneStep Organization REST API.
*/

const URL = process.env.ONESTEP_ORG_URL;
const ID_PERSON_BY_AUTH_TOKEN_PATH = '/persons/requester/token';
const GET_PERSON_ORG_ROLES_PATH = '/organizations/get-org-roles-by-person/';
const ORG_PATH = '/organizations/';

/**
  IDs a person via their auth token.

  @param authToken authentication token to ID
  @return response from org API containing Person object or logs error and null
*/
export async function idPersonByAuthToken(authToken) {
  try {
    const response = await axios.get(URL + ID_PERSON_BY_AUTH_TOKEN_PATH, { headers: { Authorization: authToken } } );
    return response.data;
  }
  catch (error) {
    console.error(error);
    return null;
  }
}

/**
  Get Person's organizations and roles for those orgs.

  @param personId person ID to get org/roles for
  @return response from org API containing list of orgIds/names and their roles in those orgs or logs error and null
*/
export async function getPersonOrgRoles(personId) {
  try {
    const response = await axios.get(URL + GET_PERSON_ORG_ROLES_PATH + personId);
    return response.data;
  }
  catch (error) {
    console.error(error);
    return null;
  }
}

/**
  Retrieve particular organization by ID.

  @param orgId organization uuid to retrieve
  @return response from org API containing org details or logs error and null
*/
export async function getOrg(orgId) {
  try {
    const response = await axios.get(URL + ORG_PATH + orgId);
    return response.data;
  }
  catch (error) {
    console.error(error);
    return null;
  }
}
