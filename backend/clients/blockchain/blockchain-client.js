import axios from 'axios';

const URL = process.env.ONESTEP_BLOCKCHAIN_URL;
const SEND_ALGO_PATH = "/transaction/sendAlgo";
const SEND_USDC_PATH = "/transaction/sendUsdc";

/**
    Processes a transaction for USDC.

    @param sender { paydId: "paystring$pagoservices.com" }
    @param receiver { address: "<paystring> or <blockchain address>" }
    @param amount amount to transact
    @param note note to add to blockchain txn

    @return successful transaction details or null on failure
*/
export async function sendUsdc(sender, receiver, amount, note) {
  console.log("POST: " + URL + SEND_USDC_PATH);

  // first, prepare request per API spec
  let txRequest = {
      sender: sender,
      receiver: receiver,
      amount: amount,
      note: note,
  }

  console.log(txRequest);

  try {
      const response = await axios.post(URL + SEND_USDC_PATH, txRequest);
      console.log(response);
      return response.data;
  } catch (error) {
      console.error(error);
      return null;
  }
}

/**
    Processes a transaction for ALGO.

    @param sender { paydId: "paystring$pagoservices.com" }
    @param receiver { address: "<paystring> or <blockchain address>" }
    @param amount amount to transact
    @param note note to add to blockchain txn

    @return successful transaction details or null on failure
*/
export async function sendAlgo(sender, receiver, amount, note) {
  console.log("POST: " + URL + SEND_ALGO_PATH);

  // first, prepare request per API spec
  let txRequest = {
      sender: sender,
      receiver: receiver,
      amount: amount,
      note: note,
  }

  console.log(txRequest);

  try {
      const response = await axios.post(URL + SEND_ALGO_PATH, txRequest);
      return response.data;
  } catch (error) {
      console.error(error);
      return null;
  }
}
