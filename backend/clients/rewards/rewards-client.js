import axios from 'axios';

/**
  Client to access OneStep Rewards REST API.
*/

const URL = process.env.ONESTEP_REWARDS_URL;
const ISSUE_PATH = '/reward/issue';

/**
  Issues reward to personId.

  @param reason reason for reward
  @param amount amount to reward
  @param personId person to reward
  @param serviceAuthToken authentication token for service
  @return response from rewards API containing reward details or logs error and null
*/
export async function issueReward(reason, amount, personId, serviceAuthToken) {
  let header = { headers: { "Authorization": "Bearer " + serviceAuthToken } };

  let data = {
    receiverId: personId,
	  receiverType: "Person",
    amount: amount,
    rewardType: "Step Token",
    reason: reason
  }

  console.log(data);

  try {
    const response = await axios.post(URL + ISSUE_PATH, data, header);

    console.log(response);

    return response.data;
  }
  catch (error) {
    console.error(error.message);
    return null;
  }
}
