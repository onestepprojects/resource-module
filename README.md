# resource-module

## Table of Contents
- [Team members](#team-members)
- [Product Owner](#product-owner)
- [Faculty](#faculty)
- [Technologies](#technologies)
- [Project Overview](#Project Overview)
- [Project Plan](#Project Plan)
- [Resource Module](#Resource Module)
- [How To Run Individually](#How To Run)

## Team Members

* [Erik Subatis](ers739@g.harvard.edu)
* [Gregory Retter](grr486@g.harvard.edu)
* [Joshua Markovic](jom5985@g.harvard.edu)
* [Shantha Kumari Rajendran](rajendran@g.harvard.edu)
* [Sharjil Khan](shk305@g.harvard.edu)
* [Zane Hernandez](zah693@g.harvard.edu)

## Product Owner

* Nitya Timalsina

## Faculty

* Annie Kamlang
* Eric Gieseke
* Hannah Riggs

## Technologies

* Programming Languages
    * Javascript
* Data Persistence
    * Mongo DB
* Misc.
    * Reaction Commerce

## Project Overview

This repository is set for the development of one quarter of the OneStep Relief
Platform. Specifically the code and assets stored here are for the 
organization, definition, and implementation of the platform's Fund and
Resource Modules. Each of these modules will be further elaborated on in their
own sections as seen below.

## Project Plan

1. Sprint 1
    1. Fund Module
2. Sprint 2
    1. Resource Module
3. Sprint 3
4. Sprint 4

## Resource Module

### Introduction
The Resource Module facilitates the supply and demand of various resources 
(food, wash, shelter, etc.) within the OneStep Relief Platform. 
Such function include creating and managing a resource, purchasing/selling/bartering 
resources, and leaving reviews of a resources. The module allows for 
the creation, update and management of resources, which represent a 
fa wide variety of tools to make a successful relief project. For example supplying building
materials, desks, and school supplies to rebuild a school in Nepal destroyed by an earthquake.

This E-Commerce module was based on a node E-Commerce tutorial provided on GitHub 
in part with lessons for Udemy by professor Bassir Jafarzadeh. As stated in the summary 
section repo here(https://github.com/basir/node-react-ecommerce) the original structure is
free to be changed.

### Requirements

The requirements of the OneStep Relief Platform's Resource Module are:
* Create and Manage Resources of for OneStep Relief Projects
* Allow accredited users to buy/sell resources
* Allow users to leave reviews for purchased resources

### Organization

The following outlines the layout of the resource module portion of Team 3's 
OneStep Relief Repository.

## How To Run

In order to run the OneStep Relief Resources Module you will need to install the following:

* Node.JS
* MongoDB

Once those requirements have been met follow these instructions

1. Clone the resource-module repo to your machine
2. Open three terminals
3. In the first terminal run the following command:
> brew services start mongodb-community@4.4

4. In the second and third terminal cd into the root folder and the root/frontend folder
of the cloned resource-module repo
5. In both terminals run the following command:
> npm install

6. In both terminals run the following command:
> npm start
    
