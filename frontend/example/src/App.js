import React, { useEffect, useState } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import { AuthHelper } from "@cscie599sec1spring21/authentication-helper";

import Amplify from "aws-amplify";

import { Auth, Hub } from "aws-amplify";

import amplifyConfig from "./aws-amplify.config.js";

import Resource from "resource-module";
import "resource-module/dist/index.css";

import "./shards-dashboards.1.1.0.min.css";

Amplify.configure({ Auth: amplifyConfig });
Auth.configure({ Auth: amplifyConfig });

const App = () => {
  const [user, setUser] = useState(null);
  const [person, setPerson] = useState(null);

  useEffect(() => {
    const signIn = async () => {
      try {
        setUser(await getUser());
      } catch (error) {
        Auth.federatedSignIn();
      }
    };

    signIn();

    Hub.listen("auth", async ({ payload: { event, data } }) => {
      switch (event) {
        case "signIn":
        case "cognitoHostedUI":
          setUser(await getUser());
          break;
        case "signOut":
          setUser(null);
          break;
        case "signIn_failure":
        case "cognitoHostedUI_failure":
        default:
          break;
      }
    });
  }, []);

  useEffect(() => {
    if (!user) return;

    let authHelper = new AuthHelper();

    const getUserAccess = async () => {
      var response = await authHelper.Get(
        process.env.REACT_APP_ORG_MODULE_API_URL + "/persons/requester/token"
      );

      if (response && response.status === 200 && response.data) {
        setPerson(response.data);
      }
    };

    getUserAccess();
  }, [user]);

  const getUser = async () => {
    return await Auth.currentAuthenticatedUser();
  };

  const handleRenderProps = (routerProps) => {
    let authHelper = new AuthHelper();

    return (
      <Resource {...routerProps} person={person} authHelper={authHelper} />
    );
  };

  return (
    user && (
      <div>
        <Router>
          <Switch>
            <Route path="/resource" render={handleRenderProps} />
          </Switch>
        </Router>
      </div>
    )
  );
};

export default App;
