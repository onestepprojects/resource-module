import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom-v5'
import './App.css'
import HomeScreen from './screens/HomeScreen'
import ResourceScreen from './screens/ResourceScreen'
import BuyScreen from './screens/BuyScreen'
import ResourcesScreen from './screens/ResourcesScreen'
import OrderScreen from './screens/OrderScreen'
import OrdersScreen from './screens/OrdersScreen'
import SupplierOrdersScreen from './screens/SupplierOrdersScreen'
import SupplierScreen from './screens/SupplierScreen'
import Header from './components/Header'
import '../src/styles/resource.css'
import Discussions from './components/Discussions'

const Resource = ({ match, person, authHelper, mountPath }) => {
  return (
    <BrowserRouter>
      {/* <div className="grid-container"> */}
      <Header person={person} authHelper={authHelper} mountPath={mountPath} />

      <div className='content'>
        <Switch>
          <Route
            exact
            path={match.url + '/discussions/:id'}
            render={(props) => (
              <Discussions
                {...props}
                authHelper={authHelper}
                mountPath={mountPath}
                person={person}
              />
            )}
          />
          <Route
            exact
            path={match.url}
            render={(props) => (
              <HomeScreen
                {...props}
                person={person}
                authHelper={authHelper}
                mountPath={mountPath}
              />
            )}
          />
          <Route
            exact
            path={match.url + '/resource/:id'}
            render={(props) => (
              <ResourceScreen
                {...props}
                person={person}
                authHelper={authHelper}
                mountPath={mountPath}
              />
            )}
          />
          <Route
            exact
            path={match.url + '/buy/:id/:qty'}
            render={(props) => (
              <BuyScreen
                {...props}
                authHelper={authHelper}
                mountPath={mountPath}
              />
            )}
          />
          <Route
            exact
            path={match.url + '/resources'}
            render={(props) => (
              <ResourcesScreen
                {...props}
                person={person}
                authHelper={authHelper}
                mountPath={mountPath}
              />
            )}
          />
          <Route
            exact
            path={match.url + '/orders'}
            render={(props) => (
              <OrdersScreen
                {...props}
                person={person}
                authHelper={authHelper}
                mountPath={mountPath}
              />
            )}
          />
          <Route
            exact
            path={match.url + '/order/:id'}
            render={(props) => (
              <OrderScreen
                {...props}
                person={person}
                authHelper={authHelper}
                mountPath={mountPath}
              />
            )}
          />
          <Route
            exact
            path={match.url + '/supplierorders'}
            render={(props) => (
              <SupplierOrdersScreen
                {...props}
                person={person}
                authHelper={authHelper}
                mountPath={mountPath}
              />
            )}
          />
          <Route
            exact
            path={match.url + '/supplier/:id'}
            render={(props) => (
              <SupplierScreen
                {...props}
                authHelper={authHelper}
                mountPath={mountPath}
                person={person}
              />
            )}
          />
        </Switch>
      </div>
    </BrowserRouter>
  )
}

export default Resource
