const URL = process.env.REACT_APP_RESOURCE_MODULE_API_URL

/*
    Retrieves list of orgs based on person.

    @param personId
    @return { data: [ {org1}, {org2}, etc. ] }
*/

export async function getOrgs(authHelper) {
  const path = URL + '/api/suppliers/user/supplier-org-list'
  try {
    const response = await authHelper.Get(path)
    return response
  } catch (error) {
    console.error(error)
    if (error.response) {
      return error.response
    }
  }
}

// Code snippet source https://stackoverflow.com/questions/52092873/how-do-i-make-axios-api-call-in-a-separate-component-file/52093785
export async function listResources(
  authHelper,
  callback,
  errorcallback,
  orgId = '',
  visibility = '',
  category = '',
  searchKeyword = '',
  supplierId = '',
  sortOrder = '',
  personId = ''
) {
  await authHelper
    .Get(
      URL +
        '/api/resources?category=' +
        category +
        '&visibility=' +
        visibility +
        '&searchKeyword=' +
        searchKeyword +
        '&supplierId' +
        supplierId +
        '&sortOrder=' +
        sortOrder +
        '&orgId=' +
        orgId +
        '&personId=' +
        personId
    )
    .then((res) => {
      // do something based on the callback method provided
      if (callback != null) {
        console.log(res)
        callback(res)
      }
    })
    .catch((err) => {
      // catch error
      if (errorcallback != null) {
        errorcallback(err)
      }
    })
}

export async function saveResource(authHelper, resource) {
  // Create new resource
  if (!resource._id) {
    console.log('Creating')
    try {
      const res = await authHelper.Post(URL + '/api/resources', resource)
      return res
    } catch (err) {
      console.log(err)
      if (err.response) {
        return err.response
      }
    }

    // Update existing resource
  } else {
    console.log('Updating: ' + resource._id)
    try {
      const res = await authHelper.Put(
        URL + '/api/resources/' + resource._id,
        resource
      )
      return res
    } catch (err) {
      console.log(err)
      if (err.response) {
        return err.response
      }
    }
  }
}

export async function deleteResource(authHelper, resourceId, errorcallback) {
  // await axios.delete(URL+'/api/resources/' + resourceId, {
  //   headers: {
  //     Authorization: 'Bearer ' + "userInfo.token",
  //   },
  // })
  await authHelper.Delete(URL + '/api/resources/' + resourceId).catch((err) => {
    // catch error
    if (errorcallback != null) {
      errorcallback(err)
    }
  })
}

export async function detailsResource(
  authHelper,
  resourceId,
  callback,
  errorcallback
) {
  await authHelper
    .Get(URL + '/api/resources/' + resourceId)
    .then((res) => {
      // do something based on the callback method provided
      if (callback != null) {
        console.log(res)
        callback(res)
      }
    })
    .catch((err) => {
      // catch error
      if (errorcallback != null) {
        errorcallback(err)
      }
    })
}

export async function incrementResourceViews(authHelper, resourceId) {
  try {
    const res = await authHelper.Get(
      URL + '/api/resources/increment-views/' + resourceId
    )
    return res
  } catch (err) {
    if (err.response) {
      console.log(err.response)
      return err.response
    }
  }
}

export async function saveResourceReview(
  authHelper,
  resourceId,
  review,
  errorcallback
) {
  await authHelper
    .Post(URL + `/api/resources/reviews/` + resourceId, review)
    .catch((err) => {
      // catch error
      if (errorcallback != null) {
        errorcallback(err)
      }
    })
}

export async function uploadResourceImage(
  authHelper,
  fileData,
  resourceId,
  callback,
  errorcallback
) {
  console.log(
    'Uploading image for: ' +
      resourceId +
      ' using URL: ' +
      URL +
      '/api/resources/image/'
  )

  await authHelper
    .Put(URL + '/api/resources/image/' + resourceId, fileData)
    .then((res) => {
      console.log(res)

      if (callback != null) {
        callback(res)
      }
    })
    .catch((err) => {
      // catch error
      console.log(err.message)

      if (errorcallback != null) {
        errorcallback(err)
      }
    })
}

export function isAdmin(orgs) {
  const adminRoles = ['GLOBAL_ADMIN', 'ADMIN', 'RELIEFORGADMIN']
  for (const org of orgs) {
    for (const role of org.roles) {
      // If role is an admin role then check orgID if required
      if (adminRoles.includes(role)) {
        return true
      }
    }
  }
  return false
}
