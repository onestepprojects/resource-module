const URL = process.env.REACT_APP_ORG_MODULE_API_URL

export async function getOrg(authHelper, orgId) {
  try {
    const res = await authHelper.Get(URL + '/organizations/' + orgId)
    return res
  } catch (err) {
    if (err.response) {
      console.log(err.response)
      return err.response
    }
  }
}
