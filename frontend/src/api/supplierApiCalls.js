const URL = process.env.REACT_APP_RESOURCE_MODULE_API_URL

export async function saveSupplier(authHelper, supplier, errorcallback) {
  // Create new supplier
  if (!supplier._id) {
    console.log('Creating')
    await authHelper
      .Post(URL + '/api/suppliers', supplier, {
        headers: {
          Authorization: 'Bearer ' + 'userInfo.token'
        }
      })
      .catch((err) => {
        // catch error
        if (errorcallback != null) {
          errorcallback(err)
        }
      })

    // Update existing supplier
  } else {
    console.log('Updating: ' + supplier._id)
    await authHelper
      .Put(URL + '/api/suppliers/' + supplier._id, supplier, {
        headers: {
          Authorization: 'Bearer ' + 'userInfo.token'
        }
      })
      .catch((err) => {
        // catch error
        if (errorcallback != null) {
          errorcallback(err)
        }
      })
  }
}

export async function getSupplier(authHelper, supplierId) {
  try {
    const res = await authHelper.Get(URL + '/api/suppliers/' + supplierId)
    return res
  } catch (err) {
    if (err.response) {
      console.log(err.response)
      return err.response
    }
  }
}

export async function getSupplierByOrgId(
  authHelper,
  orgId,
  callback,
  errorcallback
) {
  await authHelper
    .Get(URL + '/api/suppliers/orgid/' + orgId)
    .then((res) => {
      // do something based on the callback method provided
      if (callback != null) {
        console.log(res)
        callback(res)
      }
    })
    .catch((err) => {
      // catch error
      if (errorcallback != null) {
        errorcallback(err)
      }
    })
}

export async function saveSupplierReview(
  authHelper,
  supplierId,
  review,
  errorcallback
) {
  await authHelper
    .Post(URL + '/api/suppliers/' + supplierId + '/reviews/', review, {
      headers: {
        Authorization: 'Bearer ' + 'userInfo.token'
      }
    })
    .catch((err) => {
      // catch error
      if (errorcallback != null) {
        errorcallback(err)
      }
    })
}
