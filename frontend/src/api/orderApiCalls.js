/* eslint-disable camelcase */
import Axios from 'axios'

const URL = process.env.REACT_APP_RESOURCE_MODULE_API_URL

// Code snippet source https://stackoverflow.com/questions/52092873/how-do-i-make-axios-api-call-in-a-separate-component-file/52093785
export async function listOrders(
  authHelper,
  callback,
  errorcallback,
  supplierId = '',
  orgId = '',
  personId = '',
  searchKeyword = '',
  sortOrder = ''
) {
  const api_path =
    URL +
    '/api/orders?searchKeyword=' +
    searchKeyword +
    '&supplierId=' +
    supplierId +
    '&sortOrder=' +
    sortOrder +
    '&orgId=' +
    orgId +
    '&personId=' +
    personId
  console.log(api_path)
  await authHelper
    .Get(api_path)
    .then((res) => {
      // do something based on the callback method provided
      if (callback != null) {
        callback(res)
      }
    })
    .catch((err) => {
      // catch error
      if (errorcallback != null) {
        errorcallback(err)
      }
    })
}

export function saveOrder(authHelper, order, callback, errorcallback) {
  // Create new order
  if (!order._id) {
    console.log('Creating')
    authHelper
      .Post(URL + '/api/orders', order)
      .then((res) => {
        // do something based on the callback method provided
        if (callback != null) {
          console.log(res)
          callback(res)
        }
      })
      .catch((err) => {
        // catch error
        if (errorcallback != null) {
          errorcallback(err)
        }
      })

    // Update existing order - TODO: only order state can be updated. but this functionality is not currently available in frontend
  } else {
    console.log('Updating: ' + order._id)
    Axios.put(URL + '/api/orders/' + order._id, order, {
      headers: {
        Authorization: 'Bearer ' + 'userInfo.token'
      }
    })
      .then((res) => {
        // do something based on the callback method provided
        if (callback != null) {
          console.log(res)
          callback(res)
        }
      })
      .catch((err) => {
        // catch error
        if (errorcallback != null) {
          errorcallback(err)
        }
      })
  }
}

export async function getOrder(authHelper, orderId) {
  try {
    const res = await authHelper.Get(URL + '/api/orders/' + orderId)
    return res
  } catch (err) {
    if (err.response) {
      console.log(err.response)
      return err.response
    }
  }
}

export async function payOrder(authHelper, orderId, payId) {
  try {
    const res = authHelper.Post(URL + '/api/orders/pay', {
      orderId: orderId,
      payId: payId
    })
    return res
  } catch (err) {
    if (err.response) {
      console.log(err.response)
      return err.response
    }
  }
}
