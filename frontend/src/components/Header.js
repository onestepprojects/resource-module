import React, { Component } from 'react'
import { Link } from 'react-router-dom-v5'
import { getOrgs, isAdmin } from '../api/resourceApiCalls'

export class Header extends Component {
  constructor(props) {
    super(props)
    this.state = {
      orgs: []
    }
  }

  // Executes when the page loads and gets all the resources.
  componentDidMount() {
    // Get the orgs for the logged in User. If available at mount.
    if (this.props.authHelper) {
      // Get orgs
      getOrgs(this.props.authHelper).then((res) => {
        if (res) {
          this.setState({ orgs: res.data })
        }
      })
    }
  }

  // After mount if anything changes this method executes.
  componentDidUpdate(prevProps) {
    // Check if Auth helper is available and pull orgs now.
    if (this.props.authHelper !== prevProps.authHelper) {
      // Get orgs
      getOrgs(this.props.authHelper).then((res) => {
        if (res) {
          this.setState({ orgs: res.data })
        }
      })
    }
  }

  render() {
    return (
      <header className='header'>
        <div className='brand'></div>
        <div className='header-links'>
          <div className='discussion'>
            <Link to={`${this.props.mountPath}/discussions/resources`}>
              <button type='button' className='btn btn-primary btn-sm'>
                Discussion Board
              </button>
            </Link>
          </div>
          <Link to={`${this.props.mountPath}/orders`}>
            <button type='button' className='btn btn-primary btn-sm'>
              My Orders
            </button>
          </Link>
          {this.state.orgs
            ? isAdmin(this.state.orgs) && (
                <div className='dropdown'>
                  <a href='#'>
                    {' '}
                    <button type='button' className='btn btn-secondary btn-sm'>
                      Admin
                    </button>
                  </a>
                  <ul className='dropdown-content'>
                    <li>
                      <Link to={`${this.props.mountPath}/supplierorders`}>
                        Orders
                      </Link>
                      <Link to={`${this.props.mountPath}/resources`}>
                        Resources
                      </Link>
                    </li>
                  </ul>
                </div>
              )
            : ''}
        </div>
      </header>
    )
  }
}

export default Header
