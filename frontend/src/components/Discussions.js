import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom-v5'

import DiscussionBoard from 'react-discussion-board'

import 'react-discussion-board/dist/index.css'
import {
  addPostToDiscussion,
  getComments,
  newPost
} from '../services/api/models/discussion'
// import { getSolution } from '../services/api/models/solutions'
import { getCurrentUser } from '../services/api/models/auth'
import { publishNode } from '../services/api/models/nodes'

// ROUTE: /solutions/category/:category/:subcategory/:solution/discussion/general
// ROUTE: /solutions/category/:category/:subcategory/:solution/discussion/implementations
const Discussions = ({ match, authHelper }) => {
  // TODO: Work on styling
  // TODO: automated messages
  // TODO: Shortcuts to messages

  const [posts, setPosts] = useState([])
  const [curUser, setCurUser] = useState(null)
  const [board, setBoard] = useState(null)
  const { id } = useParams()

  /*   useEffect(() => {
    getSolution(authHelper, category, subcategory, solution).then((data) => {
      if (data) {
        setSolutionDetails(data)
      }
    })
  }, []) */

  useEffect(() => {
    getCurrentUser(authHelper).then((data) => {
      if (data) {
        setCurUser({
          id: data.uuid,
          firstName: data.firstname,
          username: data.username
        })
      }
    })
  }, [])

  useEffect(() => {
    if (!curUser) {
      return
    }

    getComments(authHelper, id).then((data) => {
      if (data.data.nodes.elements[0]) {
        setBoard(data.data.nodes.elements[0].uuid)
      } else {
        console.log('error - NULL ID passed')
        return
      }
      if (data.data.nodes.elements[0].fields.comments) {
        const comments = data.data.nodes.elements[0].fields.comments.map(
          (comment) => {
            return {
              uuid: comment.uuid,
              profileImage:
                'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
              name: comment.creator.firstname
                ? comment.creator.firstname
                : comment.creator.username,
              content: comment.fields.value,
              date: new Date(comment.created)
            }
          }
        )

        setPosts(comments)
      }
    })
  }, [curUser])

  const submitPost = async (text) => {
    const data = await newPost(authHelper, text)

    await publishNode(authHelper, data.uuid, data.language)

    const discussion = await addPostToDiscussion(
      authHelper,
      data.uuid,
      board,
      posts.map((post) => {
        return { uuid: post.uuid }
      })
    )

    await publishNode(authHelper, discussion.uuid, discussion.language)

    const curDate = new Date()

    setPosts([
      ...posts,
      {
        uuid: data.uuid,
        profileImage:
          'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
        name: curUser.firstName ? curUser.firstName : curUser.username,
        content: text,
        date: curDate
      }
    ])
  }

  return (
    <React.Fragment>
      {board ? (
        <div className='main-content-container container-fluid px-4'>
          <div
            className='page-header row no-gutters py-4 mb-3 border-bottom justify-content-between'
            style={{ position: 'relative' }}
          >
            <div className='col-10 col-sm-4 text-center text-sm-left mb-0'>
              <span className='text-uppercase page-subtitle'>Resources</span>
              <h3 className='page-title'>Discussion Board</h3>
            </div>
          </div>
          <DiscussionBoard posts={posts} onSubmit={submitPost} />
        </div>
      ) : (
        <p>Not found</p>
      )}
    </React.Fragment>
  )
}

export default Discussions
