import React, { Component } from 'react'
import { Map, GoogleApiWrapper, InfoWindow, Marker } from 'google-maps-react'

const GOOGLE_API_KEY = process.env.REACT_APP_RESOURCE_GOOGLEMAPS_API_KEY

const mapStyles = {
  width: '525px',
  height: '425px'
}

// source: https://www.digitalocean.com/community/tutorials/how-to-integrate-the-google-maps-api-into-react-applications

export class MapContainer extends Component {
  constructor(props) {
    super(props)

    this.state = {
      showingInfoWindow: false, // Hides or shows the InfoWindow
      activeMarker: {}, // Shows the active marker upon click
      selectedPlace: {}, // Shows the InfoWindow to the selected place upon a marker

      lat: this.props.lat,
      long: this.props.long,
      address: this.props.address
    }
  }

  componentDidMount() {
    this.setState({ lat: this.props.lat, long: this.props.long })
  }

  onMarkerClick(props, marker, e) {
    this.setState({
      selectedPlace: props,
      activeMarker: marker,
      showingInfoWindow: true
    })
  }

  onClose(props) {
    if (this.state.showingInfoWindow) {
      this.setState({
        showingInfoWindow: false,
        activeMarker: null
      })
    }
  }

  render() {
    return (
      <Map
        google={this.props.google}
        zoom={12}
        style={mapStyles}
        initialCenter={{
          lat: this.state.lat,
          lng: this.state.long
        }}
      >
        <Marker onClick={this.onMarkerClick} name={this.state.address} />
        <InfoWindow
          marker={this.state.activeMarker}
          visible={this.state.showingInfoWindow}
          onClose={this.onClose}
        >
          <div>
            <h4>{this.state.selectedPlace.name}</h4>
          </div>
        </InfoWindow>
      </Map>
    )
  }
}

export default GoogleApiWrapper({
  apiKey: GOOGLE_API_KEY
})(MapContainer)
