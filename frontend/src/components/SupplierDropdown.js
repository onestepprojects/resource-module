import React from 'react'

// Source Code Example used: https://gist.github.com/whoisryosuke/578be458b5fdb4e71b75b205608f3733
export class SupplierDropdown extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      selected_supplier: 'loading',
      suppliers: [],
      isFetching: false
    }
  }

  // TODO: pass auth token to get users
  getSuppliers() {
    this.setState({ ...this.state, isFetching: true })

    // axios.get(API.GET_SUPPLIERS_etc...) {...}

    // for now:
    // this.setState({"selected_supplier: "})... // TODO
  }

  handleChange(event) {
    this.setState({ selected_supplier: event.target.value })
  }

  render() {
    return (
      <select>
        <option key='Home Depot' value='Home Depot'>
          Home Depot
        </option>
        <option key='Salvation Army' value='Salvation Army'>
          Salvation Army
        </option>
      </select>
    )
  }
}
