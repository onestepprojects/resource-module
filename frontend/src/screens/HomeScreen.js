import React from 'react'
import { Link } from 'react-router-dom-v5'
import { listResources } from '../api/resourceApiCalls'
import Rating from '../components/Rating'
import { Card } from 'shards-react'

import numeral from 'numeral'

const ALGO_USDC_SCALE = 1 / 1000000

class HomeScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      resources: [],
      supplierId: '',
      visibility: '',
      searchKeyword: '',
      sortOrder: '',
      category: '',
      personLoc: {}
    }
  }

  // Sets search keyword state
  setSearchKeyword(keyword) {
    this.setState({ searchKeyword: keyword })
  }

  // Sets sort order state.
  setSortOrder(order) {
    this.setState({ sortOrder: order })
  }

  // Sets category state.
  setCategory(cat) {
    this.setState({ category: cat })
  }

  // Sets visibility state.
  setVisibility(vis) {
    this.setState({ visibility: vis })
  }

  // Sets supplierId state
  setSupplierId(supId) {
    this.setState({ supplierId: supId })
  }

  // Submit handler for search
  submitHandler(e) {
    e.preventDefault()
    listResources(
      this.props.authHelper,
      (res) => this.setState({ resources: res.data }), // Call back method used to set the state
      (err) => {
        alert(err)
      }, // Error call back method used to alert error.
      '',
      'PUBLISHED',
      this.state.category,
      this.state.searchKeyword,
      '',
      this.state.sortOrder,
      ''
    )
  }

  // Handler for sort
  sortHandler(e) {
    this.setSortOrder(e.target.value)
    listResources(
      this.props.authHelper,
      (res) => this.setState({ resources: res.data }), // Call back method used to set the state
      (err) => {
        alert(err)
      }, // Error call back method used to alert error.
      '',
      'PUBLISHED',
      this.state.category,
      this.state.searchKeyword,
      '',
      this.state.sortOrder,
      ''
    )
  }

  // Executes when the page loads and gets all the resources.
  componentDidMount() {
    // Get all the resources to show on the home screen.
    if (this.props.authHelper) {
      listResources(
        this.props.authHelper,
        (res) => this.setState({ resources: res.data }), // Call back method used to set the state
        (err) => {
          alert(err)
        }, // Error call back method used to alert error.
        '', // no org id
        'PUBLISHED' // visibility
      )
    }

    if (
      this.props.person !== undefined &&
      this.props.person !== null &&
      this.props.person.location
    ) {
      // get first location
      this.setState({ personLoc: this.props.person.location[0] })
    }
  }

  // If the person object changes. Or gets loaded after mount already happened. get person to calc distances
  componentDidUpdate(prevProps) {
    if (this.props.person !== prevProps.person && this.props.person.location) {
      // get first location
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({ personLoc: this.props.person.location[0] })
    }
  }

  // Converts length to radians
  // Source- https://stackoverflow.com/questions/1502590/calculate-distance-between-two-points-in-google-maps-v3
  rad(x) {
    return (x * Math.PI) / 180
  }

  // Calculates the distance between two sets of coordinates.
  // @param location - location of a resource
  // @param lat2 - latitude of the current user
  // @param long2 - longitude of the current user
  // @return d- distance calculated or 0 (unable to return a distance).
  // Source- https://stackoverflow.com/questions/1502590/calculate-distance-between-two-points-in-google-maps-v3
  getDistance(resourceLocation, personLocation) {
    if (
      resourceLocation &&
      personLocation &&
      resourceLocation.long &&
      resourceLocation.lat &&
      personLocation.longitude &&
      personLocation.latitude
    ) {
      const R = 3958.8 // Earth’s mean radius in miles

      const location1 = {
        lat: resourceLocation.lat,
        long: resourceLocation.long
      }
      const location2 = {
        lat: personLocation.latitude,
        long: personLocation.longitude
      }

      // Get the median latitude and longitude between two locations
      const dLat = this.rad(location2.lat - location1.lat)
      const dLong = this.rad(location2.long - location1.long)

      const a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(this.rad(location1.lat)) *
          Math.cos(this.rad(location2.lat)) *
          Math.sin(dLong / 2) *
          Math.sin(dLong / 2)

      const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
      const d = R * c

      // Returns the distance between the two coordinate sets in miles.
      // Formats the number with commas and no decimal places.
      // Format provided by: https://stackoverflow.com/questions/44784774/in-react-how-to-format-a-number-with-commas/44784907
      return (
        d.toLocaleString(undefined, { maximumFractionDigits: 0 }) + ' miles'
      )
    } else return ' '
  }

  render() {
    return (
      <div>
        <h3 className='resources-header'>Available Resources</h3>
        <div></div>
        <div className='search-sort'>
          <div className='sort'>
            Sort By{' '}
            <select name='sortOrder' onChange={this.sortHandler}>
              <option value=''></option>
              <option value=''>Newest</option>
              <option value='highest'>Lowest</option>
              <option value='lowest'>Highest</option>
            </select>
          </div>

          <div className='filter'>
            <form onSubmit={this.submitHandler}>
              <input
                name='searchKeyword'
                onChange={(e) => this.setSearchKeyword(e.target.value)}
              />
              <button type='submit'>Search</button>
            </form>
          </div>
        </div>
        <ul className='resources'>
          {this.state.resources.map((resource) => (
            <Card key={resource._id} small className='mb-4 resource-cards'>
              <li>
                <div className='col-lg-6 col-md-6 col-sm-12 mb-4'>
                  <div className='resource'>
                    <Link
                      to={`${this.props.mountPath}/resource/${resource._id}`}
                    >
                      <img
                        className='resource-image'
                        src={resource.image}
                        alt='resource'
                      />
                    </Link>
                    <div className='resource-name'>
                      <Link
                        to={`${this.props.mountPath}/resource/${resource._id}`}
                      >
                        {resource.name}
                      </Link>
                    </div>
                    <div className='resource-brand'>{resource.brand}</div>
                    <div className='resource-price'>
                      {numeral(resource.price * ALGO_USDC_SCALE).format(
                        '0,0.00'
                      )}{' '}
                      {resource.currency}
                    </div>
                    <div className='resource-brand'>
                      {resource.shippingOption}
                    </div>
                    <div className='resource-rating'>
                      <Rating value={resource.rating} />
                    </div>
                  </div>
                </div>
              </li>
              <div className='resource-bottom'>
                <div className='resource-views'>
                  Views: {numeral(resource.views).format('0,0')}
                </div>

                <div className='resource-loc'>
                  {this.getDistance(resource.location, this.state.personLoc)}
                </div>
              </div>
            </Card>
          ))}
        </ul>
      </div>
    )
  }
}
export default HomeScreen
