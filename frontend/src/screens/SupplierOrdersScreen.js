import React from 'react'
import { Link } from 'react-router-dom-v5'
import { listOrders } from '../api/orderApiCalls'
import { getOrgs } from '../api/resourceApiCalls'

import numeral from 'numeral'

const ALGO_USDC_SCALE = 1 / 1000000

class OrdersScreen extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      orgs: [],
      orders: [],
      selectedOrders: [],
      selected: 'All'
    }
  }

  // Executes when the page loads and gets all the resources.
  componentDidMount() {
    // Get the orgs for the logged in User. If available at mount.
    if (this.props.authHelper) {
      // Get orgs
      getOrgs(this.props.authHelper).then((res) => {
        if (res) {
          this.setState({ orgs: res.data })
        }
      })
    }
  }

  // After mount if anything changes this method executes.
  componentDidUpdate(prevProps, prevState) {
    // Check if Auth helper is available and pull orgs now.
    if (this.props.authHelper !== prevProps.authHelper) {
      // Get orgs
      getOrgs(this.props.authHelper).then((res) => {
        if (res) {
          this.setState({ orgs: res.data })
        }
      })
    }

    // We received orgs. So we can now retreive the orders.
    if (prevState.orgs !== this.state.orgs) {
      for (const org of this.state.orgs) {
        listOrders(
          this.props.authHelper,
          (res) =>
            this.setState({ orders: this.state.orders.concat(res.data) }), // Call back method used to set the state
          (err) => {
            alert(err)
          }, // Error call back method used to alert error.
          '',
          org.id // Org Ids
        )
      }
    }

    // If the selected org changes, Retreive the selected org orders.
    if (prevState.selected !== this.state.selected) {
      listOrders(
        this.props.authHelper,
        (res) => this.setState({ selectedOrders: res.data }), // Call back method used to set the state
        (err) => {
          alert(err)
        }, // Error call back method used to alert error.
        '',
        this.state.selected // Id for the selected Org
      )
    }
  }

  // Changes which supplier is selected.
  handleSelect(e) {
    console.log(this.state.selected)
    this.setState({ selected: e })
  }

  // This method generates all the rows for the adming orders view.
  displayOrders(selected) {
    let displayOrders = this.state.orders

    if (selected === 'All') {
      displayOrders = this.state.orders
    } else {
      displayOrders = this.state.selectedOrders
    }
    return displayOrders.map((order) => (
      <tr key={order._id}>
        <td>
          <Link to={`${this.props.mountPath}/order/${order._id}`}>
            {order._id}
          </Link>
        </td>
        <td>
          <Link to={`${this.props.mountPath}/resource/${order.resource}`}>
            {order.resource}
          </Link>
        </td>
        <td>{order.orderState}</td>
        <td>{order.qty}</td>
        <td>{numeral(order.totalCost * ALGO_USDC_SCALE).format('0,0.00')}</td>
        <td>{order.shippingOption}</td>
        <td>
          <Link to={`${this.props.mountPath}/supplier/${order.supplierId}`}>
            {this.orglookup(order.orgId)}
          </Link>
        </td>
      </tr>
    ))
  }

  // Lookup orgname from orgId
  orglookup(orgId) {
    for (const org of this.state.orgs) {
      if (org.id === orgId) return org.name
    }
    return 'Supplier/Org unknown'
  }

  render() {
    return (
      <div className='content content-margined'>
        <div className='order-header'>
          {this.props.person ? (
            <h6> Order List for Supplier: {this.props.person.name}</h6>
          ) : (
            <h6>Orders</h6>
          )}
        </div>
        <select onChange={(e) => this.handleSelect(e.target.value)}>
          <option key='All' defaultValue='All'>
            All
          </option>
          {this.props.person &&
            this.state.orgs.map((org, index) => (
              <option key={org.name} value={org.id}>
                {org.name}
              </option>
            ))}
        </select>

        <div className='order-list'>
          <table className='table'>
            <thead>
              <tr>
                <th>ID</th>
                <th>Resource ID</th>
                <th>Order State</th>
                <th>Quantity</th>
                <th>Total Price</th>
                <th>Shipping Option</th>
                <th>Supplier/Org</th>
              </tr>
            </thead>
            <tbody>{this.displayOrders(this.state.selected)}</tbody>
          </table>
        </div>
      </div>
    )
  }
}
export default OrdersScreen
