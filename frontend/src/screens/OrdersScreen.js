import React from 'react'
import { Link } from 'react-router-dom-v5'
import { listOrders } from '../api/orderApiCalls'

import numeral from 'numeral'

const ALGO_USDC_SCALE = 1 / 1000000

class OrdersScreen extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      orders: [],
      selected: 'All'
    }
  }

  // Executes when the page loads and gets all the orders.
  componentDidMount() {
    console.log('orders mount')
    if (this.props.person) {
      console.log('orders mount - person available')
      // Get all the orders and display as a list.
      listOrders(
        this.props.authHelper,
        (res) => {
          this.setState({ orders: res.data })
          console.log(res)
        }, // Call back method used to set the state
        (err) => {
          alert(err)
        }, // Error call back method used to alert error.
        '',
        '',
        this.props.person.uuid
      )
    }
  }

  componentDidUpdate(prevProps) {
    console.log('orders update')
    if (this.props.person !== prevProps.person) {
      // Get all the orders and display as a list.
      listOrders(
        this.props.authHelper,
        (res) => {
          this.setState({ orders: res.data })
        }, // Call back method used to set the state
        (err) => {
          alert(err)
        }, // Error call back method used to alert error.
        '',
        '',
        this.props.person.uuid
      )
    }
  }

  // This method generates all the rows for the adming orders view.
  displayOrders(selected) {
    return this.state.orders.map((order) => (
      <tr key={order._id}>
        <td>
          <Link to={`${this.props.mountPath}/order/${order._id}`}>
            {order._id}
          </Link>
        </td>
        <td>
          <Link to={`${this.props.mountPath}/resource/${order.resource}`}>
            {order.resource}
          </Link>
        </td>
        <td>{order.orderState}</td>
        <td>{order.qty}</td>
        <td>{numeral(order.totalCost * ALGO_USDC_SCALE).format('0,0.00')}</td>
        <td>{order.shippingOption}</td>
        <td>
          <Link to={`${this.props.mountPath}/supplier/${order.supplierId}`}>
            {order.orgId}
          </Link>
        </td>
      </tr>
    ))
  }

  render() {
    return (
      <div className='content content-margined'>
        <div className='order-header'>
          {this.props.person ? (
            <h6> {this.props.person.name}&apos;s Orders</h6>
          ) : (
            <h6>Orders</h6>
          )}
        </div>

        <div className='order-list'>
          <table className='table'>
            <thead>
              <tr>
                <th>Order ID</th>
                <th>Resource ID</th>
                <th>Order State</th>
                <th>Quantity</th>
                <th>Total Price</th>
                <th>Shipping Option</th>
                <th>Org/Supplier</th>
              </tr>
            </thead>
            <tbody>{this.displayOrders(this.state.selected)}</tbody>
          </table>
        </div>
      </div>
    )
  }
}
export default OrdersScreen
