import React from 'react'
import { Link, withRouter } from 'react-router-dom-v5'
import { Card, Row } from 'shards-react'
import {
  detailsResource,
  saveResourceReview,
  incrementResourceViews
} from '../api/resourceApiCalls'
import { getOrg } from '../api/orgApiCalls'
import {
  getDiscussion,
  createDiscussion
} from '../services/api/models/discussion'
import { publishNode } from '../services/api/models/nodes'

import MapContainer from '../components/Map'
import Rating from '../components/Rating'

import numeral from 'numeral'

const ALGO_USDC_SCALE = 1 / 1000000

class ResourceScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      id: this.props.match.params.id,
      resource: {
        shippingOption: 'LOADING...',
        location: {
          lat: null,
          long: null,
          address: null,
          city: null,
          country: null,
          postalCode: null
        },
        reviews: []
      },
      rating: '',
      comment: '',
      qty: 1,
      supplierName: ''
    }
  }

  reviewsubmitHandler(e) {
    e.preventDefault()
    saveResourceReview(this.props.authHelper, this.state.id, {
      rating: this.state.rating,
      comment: this.state.comment
    })
    window.location.reload()
  }

  submitHandler(e) {
    e.preventDefault()
  }

  componentDidMount() {
    if (this.props.authHelper) {
      // Get all the resources to show on the home screen.
      detailsResource(
        this.props.authHelper,
        this.state.id,
        (res) => this.setState({ resource: res.data }), // Call back method used to set the state
        (err) => {
          alert(err)
        } // Error call back method used to alert error.
      )

      // increment resource views
      incrementResourceViews(this.props.authHelper, this.state.id)
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.authHelper !== prevProps.authHelper) {
      // Get all the resources to show on the home screen.
      detailsResource(
        this.props.authHelper,
        this.state.id,
        (res) => this.setState({ resource: res.data }), // Call back method used to set the state
        (err) => {
          alert(err)
        } // Error call back method used to alert error.
      )

      // increment resource views
      incrementResourceViews(this.props.authHelper, this.state.id)
    }

    if (this.state.resource !== prevState.resource) {
      getOrg(this.props.authHelper, this.state.resource.orgId).then((res) => {
        if (res.status < 200 || res.status >= 300) {
          this.setState({ supplierName: 'Supplier organization not found' })
        } else {
          this.setState({ supplierName: res.data.name })
        }
      })
    }
  }

  // discussion board logic - upon access, attempts to get the board, and creates on the fly if doesn't exist, then links
  handleDiscussions() {
    console.log('Discussion board clicked')

    getDiscussion(this.props.authHelper, this.state.id).then((data) => {
      console.log('Attempting to retrieve discussion for ID: ' + this.state.id)

      // if discussion board doesn't exist, create, and then link upon completion
      if (data.data.nodes.elements.length === 0) {
        console.log(
          'Not found. Attempting to create discussion for ID: ' + this.state.id
        )

        createDiscussion(
          this.props.authHelper,
          this.state.id,
          this.state.resource.name
        ).then((value) => {
          console.log(
            'Success. Publishing with ID: ' +
              this.state.id +
              'and uuid: ' +
              value.uuid
          )
          publishNode(this.props.authHelper, value.uuid, value.language).then(
            (res) => {
              this.props.history.push(
                this.props.mountPath + '/discussions/' + this.state.id
              )
            }
          )
        })
      } else {
        this.props.history.push(
          this.props.mountPath + '/discussions/' + this.state.id
        )
      }
    })
  }

  render() {
    const resource = this.state.resource
    const noLoc =
      'This resource does not have a location address yet. Please, check back again later.'

    return (
      <div>
        <div className='details'>
          <Row>
            <div className='details-image'>
              <img src={resource.image} alt='resource'></img>
            </div>
            <Card className='details-info'>
              <div className='details-info-inner'>
                <ul>
                  <li>
                    <h3>{resource.name}</h3>
                  </li>
                  <li>
                    <a href='#reviews'>
                      <Rating
                        value={resource.rating}
                        text={resource.numReviews + ' reviews'}
                      />
                    </a>
                  </li>
                  <li>
                    <b>
                      {numeral(resource.price * ALGO_USDC_SCALE).format(
                        '0,0.00'
                      )}{' '}
                      {resource.currency}
                    </b>
                  </li>
                  <li>
                    <h6 className='mb-0'>Description</h6>
                    <div>{resource.description}</div>
                  </li>
                  <li>
                    <h6 className='mb-0'>Supplier</h6>
                    <div>
                      <Link
                        to={`${this.props.mountPath}/supplier/${resource.supplierId}`}
                      >
                        {this.state.supplierName}
                      </Link>
                    </div>
                  </li>
                  <li>
                    <div className='discussion_link'>
                      {/* discussions only appear when a person is logged in, but dont need admin privileges */}
                      {this.props.person && (
                        <button
                          type='button'
                          className='btn btn-outline-primary btn-pill'
                          onClick={this.handleDiscussions}
                        >
                          Discussion Board
                        </button>
                      )}
                    </div>
                  </li>
                </ul>
              </div>
            </Card>
            <div className='details-action'>
              <ul>
                <li>
                  {numeral(resource.price * ALGO_USDC_SCALE).format('0,0.00')}{' '}
                  {resource.currency}
                </li>
                <li>
                  {' '}
                  {resource.countInStock > 0
                    ? 'In Stock (' +
                      numeral(resource.countInStock).format('0,0') +
                      ' left)'
                    : 'Unavailable.'}
                </li>
                <li>
                  Qty:{' '}
                  <select
                    value={this.state.qty}
                    onChange={(e) => this.setState({ qty: e.target.value })}
                  >
                    {[...Array(resource.countInStock).keys()].map((x) => (
                      <option key={x + 1} value={x + 1}>
                        {x + 1}
                      </option>
                    ))}
                  </select>
                </li>
                <li>
                  Shipping Option(s): {this.state.resource.shippingOption}
                </li>
                <li>
                  {resource.countInStock > 0 && (
                    <Link
                      to={`${this.props.mountPath}/buy/${resource._id}/${this.state.qty}`}
                    >
                      <button
                        className='button primary'
                        onClick={this.handleBuy}
                      >
                        Buy now
                      </button>
                    </Link>
                  )}
                </li>
              </ul>
            </div>
          </Row>
          <Row>
            <div className='reviewscard'>
              <Card medium className='mb-8'>
                <div className='reviewsheader'>Reviews</div>

                {!resource.reviews.length && <div>There is no review</div>}
                <ul className='review' id='reviews'>
                  {resource.reviews.map((review) => (
                    <li key={review._id}>
                      <div id='reviewperson'>{review.name} </div>
                      <div id='reviewtime'>
                        {review.createdAt.substring(0, 10)}
                      </div>
                      <div>
                        <Rating value={review.rating}></Rating>
                      </div>

                      <div id='reviewcomments'>{review.comment}</div>
                    </li>
                  ))}
                  <li>
                    <h6>Write a customer review</h6>
                    {this.props.person ? (
                      <form onSubmit={this.reviewsubmitHandler}>
                        <ul className='form-container'>
                          <li>
                            <label htmlFor='rating'>Rating</label>
                            <select
                              name='rating'
                              id='rating'
                              value={this.state.rating}
                              onChange={(e) =>
                                this.setState({ rating: e.target.value })
                              }
                            >
                              <option value='0'></option>
                              <option value='1'>1 - Poor</option>
                              <option value='2'>2 - Fair</option>
                              <option value='3'>3 - Good</option>
                              <option value='4'>4 - Very Good</option>
                              <option value='5'>5 - Excelent</option>
                            </select>
                          </li>
                          <li>
                            <label htmlFor='comment'>Comment</label>
                            <textarea
                              className='comment-text'
                              name='comment'
                              value={this.state.comment}
                              onChange={(e) =>
                                this.setState({ comment: e.target.value })
                              }
                            ></textarea>
                          </li>
                          <li>
                            <button type='submit' className='button primary'>
                              Submit
                            </button>
                          </li>
                        </ul>
                      </form>
                    ) : (
                      ''
                    )}
                  </li>
                </ul>
              </Card>
            </div>

            <div className='map'>
              {resource.location.lat != null &&
              resource.location.long != null &&
              resource.location.address != null ? (
                <MapContainer
                  lat={resource.location.lat}
                  long={resource.location.long}
                  address={resource.location.address}
                />
              ) : (
                noLoc
              )}
            </div>
          </Row>
        </div>
      </div>
    )
  }
}

export default withRouter(ResourceScreen)
