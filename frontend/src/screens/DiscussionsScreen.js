import React, { useEffect, useState } from 'react'
import DiscussionBoard from 'react-discussion-board'
import {
  newDiscussion,
  newPost,
  getComments,
  addPostToDiscussion
} from '../services/api/models/discussion'
import { getCurrentUser } from '../services/api/models/auth'

// import "./App.css";
import 'bootstrap/dist/css/bootstrap.min.css'
import 'react-discussion-board/dist/index.css'
import { publishNode } from '../services/api/models/nodes'

const DiscussionsScreen = (_id) => {
  const [id, setId] = useState('_id')
  const [name, setName] = useState('')
  const [posts, setPosts] = useState([])
  const [curUser, setCurUser] = useState(null)
  const [curBoard, setCurBoard] = useState(null)

  useEffect(() => {
    getCurrentUser().then((data) => {
      if (data) {
        setCurUser({
          id: data.uuid,
          firstName: data.firstname,
          lastName: data.lastname,
          username: data.username
        })
      }
    })
  }, [])

  const submitPost = async (text) => {
    const data = await newPost(text)

    await publishNode(data.uuid, data.language)

    await addPostToDiscussion(
      data.uuid,
      curBoard.uuid,
      posts.map((post) => {
        return { uuid: post.uuid }
      })
    )

    await publishNode(curBoard.uuid, curBoard.language)

    getComments(id).then((data) => {
      if (data.nodes.elements[0].fields.comments) {
        const comments = data.nodes.elements[0].fields.comments.map(
          (comment) => {
            return {
              uuid: comment.uuid,
              profileImage:
                'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
              name:
                curUser.firstName && curUser.lastName
                  ? curUser.firstName + ' ' + curUser.lastName
                  : curUser.username,
              content: comment.fields.value,
              date: new Date(comment.created)
            }
          }
        )

        setPosts(comments)
      }
    })
  }

  const handleSubmit = (event) => {
    event.preventDefault()

    newDiscussion(id, name).then((data) => {
      publishNode(data.uuid, data.language)
      setCurBoard(data)
    })

    getComments(id).then((data) => {
      if (data.nodes.elements[0].fields.comments) {
        const comments = data.nodes.elements[0].fields.comments.map(
          (comment) => {
            return {
              uuid: comment.uuid,
              profileImage:
                'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
              name:
                curUser.firstName && curUser.lastName
                  ? curUser.firstName + ' ' + curUser.lastName
                  : curUser.username,
              content: comment.fields.value,
              date: new Date(comment.created)
            }
          }
        )

        setPosts(comments)
      }
    })
  }

  return (
    <div className='main-content-container container-fluid px-4'>
      <div
        className='page-header row no-gutters py-4 mb-3 border-bottom justify-content-between'
        style={{ position: 'relative' }}
      >
        <div className='col-10 col-sm-4 text-center text-sm-left mb-0'>
          <span className='text-uppercase page-subtitle'>Module</span>
          <h3 className='page-title'>Discussion Board</h3>
        </div>
      </div>
      <form onSubmit={handleSubmit}>
        <div className='form-group'>
          <label htmlFor='discussionBoardName'>Discussion Board Name</label>
          <input
            type='text'
            className='form-control'
            id='discussionBoardName'
            value={name}
            onChange={(event) => setName(event.target.value)}
            placeholder='Enter Name'
          ></input>
        </div>
        <div className='form-group'>
          <label htmlFor='discussionBoardId'>Discussion Board ID</label>
          <input
            type='text'
            className='form-control'
            id='discussionBoardId'
            value={id}
            onChange={(event) => setId(event.target.value)}
            placeholder='Enter ID'
          ></input>
        </div>
        <button type='submit' className='btn btn-primary'>
          Submit
        </button>
      </form>
      <hr />
      <div className='container-fluid my-4'>
        <DiscussionBoard posts={posts} onSubmit={submitPost} />
      </div>
    </div>
  )
}

export default DiscussionsScreen
