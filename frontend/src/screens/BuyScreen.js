import React from 'react'
import { withRouter } from 'react-router-dom-v5'

import { detailsResource } from '../api/resourceApiCalls'
import { saveOrder, payOrder } from '../api/orderApiCalls'
import { Card } from 'shards-react'
import numeral from 'numeral'

const ALGO_USDC_SCALE = 1 / 1000000

class BuyScreen extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      id: this.props.match.params.id,
      order: {},
      qty: this.props.match.params.qty,
      resource: { shippingOption: 'DELIVERY' },
      location: {
        lat: '',
        long: '',
        address: '',
        city: '',
        postalCode: '',
        country: ''
      },
      payId: 'NA',

      modalVisible: false
    }
  }

  componentDidMount() {
    // get details of resource we are buying
    detailsResource(
      this.props.authHelper,
      this.state.id,
      (res) => {
        this.setState({ resource: res.data })

        // if pickup, no need to ask for shipping info. create order in system and go straight to payment
        if (this.state.resource.shippingOption === 'PICKUP') {
          saveOrder(
            this.props.authHelper,
            {
              resource: this.state.id,
              qty: this.state.qty,
              shippingOption: this.state.resource.shippingOption
            },
            (res) => {
              this.setState({ modalVisible: true, order: res.data })
            },
            (err) => {
              alert(err)
            }
          )
        }
      },
      (err) => {
        alert(err)
      } // Error call back method used to alert error.
    )
  }

  handleChange(event) {
    console.log(event)
    this.setState({ [event.target.name]: event.target.value })
  }

  // location is a nested attribute of state here, so this handles state change
  handleLocChange(e) {
    const newLoc = this.state.location
    newLoc[e.target.name] = e.target.value
    this.setState({ location: newLoc })
  }

  handleCreateOrder(e) {
    saveOrder(
      this.props.authHelper,
      {
        resource: this.state.id,
        qty: this.state.qty,
        shippingOption: this.state.resource.shippingOption,
        shipToLoc: this.state.location
      },
      (res) => {
        this.setState({ modalVisible: true, order: res.data })
      },
      (err) => {
        alert(err)
      }
    )
  }

  handlePay(e) {
    payOrder(
      this.props.authHelper,
      this.state.order._id,
      this.state.payId
    ).then((res) => {
      if (res.status < 200 || res.status >= 300) {
        alert(res.data.message)
      } else {
        alert('Thank you for your order!')
        this.props.history.push(
          this.props.mountPath + '/order/' + this.state.order._id
        )
      }
    })
  }

  render() {
    return (
      <Card className='buy-card'>
        <div>
          {/* if shipping = delivery enter shipping info */}
          {!this.state.modalVisible && (
            <div className='form'>
              <ul className='form-container'>
                <li>
                  <h3>Enter Shipping Information</h3>
                </li>

                <li>
                  <div>
                    <label htmlFor='orderDetails'>
                      {this.state.qty}x {this.state.resource.name}
                    </label>
                  </div>
                </li>
                <li>
                  <div>
                    <label htmlFor='orderPrice'>
                      Total cost:{' '}
                      {numeral(
                        this.state.qty *
                          this.state.resource.price *
                          ALGO_USDC_SCALE
                      ).format('0,0.00')}{' '}
                      {this.state.resource.currency}
                    </label>
                  </div>
                </li>

                <li>
                  <label htmlFor='location' style={{ fontWeight: 'bold' }}>
                    Enter Shipping Information
                  </label>
                </li>

                <li>
                  <label htmlFor='address'>Street Address</label>
                  <textarea
                    name='address'
                    value={this.state.location.address}
                    id='address'
                    onChange={this.handleLocChange}
                  ></textarea>
                </li>
                <li>
                  <label htmlFor='city'>City</label>
                  <textarea
                    name='city'
                    value={this.state.location.city}
                    id='city'
                    onChange={this.handleLocChange}
                  ></textarea>
                </li>
                <li>
                  <label htmlFor='postalCode'>Postal (zip) Code</label>
                  <textarea
                    name='postalCode'
                    value={this.state.location.postalCode}
                    id='postalCode'
                    onChange={this.handleLocChange}
                  ></textarea>
                </li>
                <li>
                  <label htmlFor='country'>Country</label>
                  <textarea
                    name='country'
                    value={this.state.location.country}
                    id='country'
                    onChange={this.handleLocChange}
                  ></textarea>
                </li>

                <li>
                  <button
                    className='button primary'
                    onClick={this.handleCreateOrder}
                  >
                    Submit
                  </button>
                </li>
              </ul>
            </div>
          )}

          {/* payment form--appears either immediately in case of pickup or after keying in shipping info */}
          {this.state.modalVisible && (
            <div className='form'>
              <ul className='form-container'>
                <li>
                  <h3>Payment</h3>
                </li>

                <li>
                  <img
                    className='buy-image'
                    src={this.state.resource.image}
                    alt='resource'
                  ></img>

                  <div>
                    <text className='buy-text'>
                      {this.state.resource.name} (
                      {this.state.resource.price * ALGO_USDC_SCALE}{' '}
                      {this.state.resource.currency})
                    </text>
                  </div>
                </li>
                <li>
                  <div>
                    <text className='buy-text'>Quantity: {this.state.qty}</text>
                  </div>
                </li>
                <li>
                  <div>
                    <text className='buy-text'>
                      Total cost:{' '}
                      {this.state.qty *
                        this.state.resource.price *
                        ALGO_USDC_SCALE}{' '}
                      {this.state.resource.currency}
                    </text>
                  </div>
                </li>

                <li>
                  <div>
                    <text className='buy-text'>Paystring</text>
                    <input
                      type='text'
                      name='payId'
                      id='payId'
                      onChange={this.handleChange}
                    ></input>
                  </div>
                </li>

                <li>
                  <button className='button primary' onClick={this.handlePay}>
                    Pay
                  </button>
                </li>
              </ul>
            </div>
          )}
        </div>
      </Card>
    )
  }
}

export default withRouter(BuyScreen)
