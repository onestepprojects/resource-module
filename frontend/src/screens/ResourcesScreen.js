import React from 'react'
import { Link } from 'react-router-dom-v5'
import { getOrg } from '../api/orgApiCalls'
import {
  listResources,
  saveResource,
  deleteResource,
  getOrgs,
  detailsResource
} from '../api/resourceApiCalls'
import ImageUpload from '../components/ImageUpload'

import { FormSelect } from 'shards-react'

const ALGO_USDC_SCALE = 1 / 1000000

class ResourcesScreen extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      orgs: [],
      resources: [],
      selectedResources: [],
      selected: 'All',
      modalVisible: false,
      _id: '',
      creatorId: '',
      orgId: '',
      name: '',
      description: '',
      category: '',
      price: '',
      fieldDisplayPrice: '',
      currency: 'USDC',
      countInStock: '',
      location: {
        lat: '',
        long: '',
        address: '',
        city: '',
        postalCode: '',
        country: ''
      },
      visibility: '',
      shippingOption: 'PICKUP',
      productCode: '',
      image: '',
      brand: '',
      note: '',
      condition: 'NEW',
      selectedOrgLoc: {
        lat: '',
        long: '',
        address: '',
        city: '',
        postalCode: '',
        country: ''
      }
    }
  }

  // Executes when the page loads and gets all the resources.
  componentDidMount() {
    // Get the orgs for the logged in User. If available at mount.
    if (this.props.authHelper) {
      // Get orgs
      getOrgs(this.props.authHelper).then((res) => {
        if (res) {
          this.setState({
            orgs: res.data,
            orgId: res.data[0].id
          })
        }
      })
    }
  }

  // After mount if anything changes this method executes.
  componentDidUpdate(prevProps, prevState) {
    // Check if Auth helper is available and pull orgs now.
    if (this.props.authHelper !== prevProps.authHelper) {
      // Get orgs
      getOrgs(this.props.authHelper).then((res) => {
        if (res) {
          this.setState({
            orgs: res.data,
            orgId: res.data[0].id
          })
        }
      })
    }

    // We received orgs. So we can now retreive the resources.
    if (prevState.orgs !== this.state.orgs) {
      for (const org of this.state.orgs) {
        listResources(
          this.props.authHelper,
          (res) =>
            this.setState({ resources: this.state.resources.concat(res.data) }), // Call back method used to set the state
          (err) => {
            alert(err)
          }, // Error call back method used to alert error.
          org.id // Org Ids
        )
      }
    }

    // If the selected org changes, Retreive the selected org resources.
    if (prevState.selected !== this.state.selected) {
      listResources(
        this.props.authHelper,
        (res) => this.setState({ selectedResources: res.data }), // Call back method used to set the state
        (err) => {
          alert(err)
        }, // Error call back method used to alert error.
        this.state.selected // Id for the selected Org
      )
    }
  }

  // Makes the Form Visible. If resource is empty it will open the create form or else the Update form.
  openModal(resource) {
    this.setState(
      {
        // Initially, no file is selected
        selectedFile: null,
        modalVisible: true,

        _id: resource._id,
        creatorId: resource.creatorId,
        supplierId: resource.orgName,
        name: resource.name,
        description: resource.description,
        category: resource.category,
        price: resource.price,
        fieldDisplayPrice: resource.price * ALGO_USDC_SCALE,
        currency: resource.currency,
        countInStock: resource.countInStock,
        location: resource.location || {
          lat: '',
          long: '',
          address: '',
          city: '',
          postalCode: '',
          country: ''
        },
        visibility: resource.visibility,
        shippingOption: resource.shippingOption,
        productCode: resource.productCode,
        image: resource.image,
        brand: resource.brand,
        note: resource.note,
        condition: resource.condition
      },
      () => {
        // attempt to populate address fields if creating new resource
        if (!resource._id) {
          getOrg(this.props.authHelper, this.state.orgId).then((res) => {
            if (res.status >= 200 && res.status < 300) {
              this.setState({
                location: {
                  lat: '',
                  long: '',
                  address: res.data.address.address,
                  city: res.data.address.city,
                  postalCode: res.data.address.postalCode,
                  country: res.data.address.country
                }
              })
            }
          })
        }
      }
    )
  }

  // Handle form submit for both Create and Update.
  // If this.state._id is not null it will update or else it will create a new resource.
  submitHandler(e) {
    e.preventDefault()

    saveResource(this.props.authHelper, {
      _id: this.state._id,
      creatorId: this.state.creatorId,
      orgId: this.state.orgId,
      name: this.state.name,
      description: this.state.description,
      category: this.state.category,
      price: this.state.price,
      currency: this.state.currency,
      countInStock: this.state.countInStock,
      location: this.state.location,
      visibility: this.state.visibility,
      shippingOption: this.state.shippingOption,
      productCode: this.state.productCode,
      image: this.state.image,
      brand: this.state.brand,
      note: this.state.note,
      condition: this.state.condition
    }).then((res) => {
      if (res.status < 200 || res.status >= 300) {
        alert(res.data.message)
      } else {
        window.location.reload()
      }
    })
  }

  // Makes the form visible or invisible based on boolean variable visible.
  setModalVisible(visible) {
    this.setState({ modalVisible: visible })
  }

  // Deletes the resource
  deleteHandler(resource) {
    deleteResource(
      this.props.authHelper,
      resource._id,
      (err) => {
        alert(err)
      } // Error call back method used to alert error.
    )
    window.location.reload()
  }

  // publishes/unpublishes a resource
  publishHandler(resource) {
    // retrieve resource first, in case its field have changed elsewhere in the modal form (e.g. from image upload)
    // this will not be necessary once modal screens are refactored into their own screens.
    detailsResource(this.props.authHelper, resource._id, (res) => {
      resource.image = this.state.image

      if (resource.visibility === 'PUBLISHED') {
        resource.visibility = 'UNPUBLISHED'
      } else {
        resource.visibility = 'PUBLISHED'
      }

      saveResource(this.props.authHelper, resource).then((res) => {
        if (res.status < 200 || res.status >= 300) {
          alert(res.data.message)
        } else {
          window.location.reload()
        }
      })
    })
  }

  // Changes which supplier is selected.
  handleSelect(e) {
    this.setState({ selected: e })
  }

  // location is a nested attribute of state here, so this handles state change
  handleLocChange(e) {
    const newLoc = this.state.location
    newLoc[e.target.name] = e.target.value
    this.setState({ location: newLoc })
  }

  handleOrgChange(event) {
    console.log('setting org ID to: ' + event.target.value)
    this.setState({ orgId: event.target.value })
  }

  handlePriceChange(event) {
    this.setState({
      price: event.target.value / ALGO_USDC_SCALE,
      fieldDisplayPrice: event.target.value
    })
  }

  // Lookup orgname from orgId
  orglookup(orgId) {
    for (const org of this.state.orgs) {
      if (org.id === orgId) return org.name
    }
    return 'Supplier/Org unknown'
  }

  // This method displays the right resources based on selection.
  displayselectedResources(selected) {
    let displayResources = this.state.resources
    if (selected === 'All') {
      displayResources = this.state.resources
    } else {
      displayResources = this.state.selectedResources
    }

    return displayResources.map((resource) => (
      <tr key={resource._id}>
        <td>
          {resource.visibility === 'PUBLISHED' ? 'PUBLISHED' : 'UNPUBLISHED'}
        </td>
        <td>
          <button
            className='mb-2 btn btn-sm btn-secondary mr-1'
            onClick={() => this.openModal(resource)}
          >
            Edit
          </button>
          <button
            className={
              resource.visibility === 'PUBLISHED'
                ? 'mb-2 btn btn-sm btn-warning mr-1'
                : 'mb-2 btn btn-sm btn-success mr-1'
            }
            onClick={() => this.publishHandler(resource)}
          >
            {resource.visibility === 'PUBLISHED' ? 'Unpublish' : 'Publish'}
          </button>
        </td>
        <td>
          <Link to={`${this.props.mountPath}/resource/${resource._id}`}>
            {resource._id}
          </Link>
        </td>
        <td>
          <Link to={`${this.props.mountPath}/resource/${resource._id}`}>
            {resource.name}
          </Link>
        </td>
        <td>{resource.price * ALGO_USDC_SCALE}</td>
        <td>{resource.currency} </td>
        <td>{resource.category}</td>
        <td>{resource.brand}</td>
        <td>
          <Link to={`${this.props.mountPath}/supplier/${resource.supplierId}`}>
            {this.orglookup(resource.orgId)}
          </Link>
        </td>
        <td>
          <button
            className='mb-2 btn btn-sm btn-outline-danger mr-1'
            onClick={() => this.deleteHandler(resource)}
          >
            Delete
          </button>
        </td>
      </tr>
    ))
  }

  updateResourceImageStateFromChild(url) {
    console.log('Updated resource state with url: ' + url)
    this.setState({ image: url })
  }

  render() {
    return (
      <div className='content content-margined'>
        <div className='resource-header'>
          {this.props.person ? (
            <h6>Manage {this.props.person.name}&apos;s Resources</h6>
          ) : (
            <h6>Resources</h6>
          )}
          <select onChange={(e) => this.handleSelect(e.target.value)}>
            <option key='All' defaultValue='All'>
              All
            </option>
            {this.props.person &&
              this.state.orgs.map((org, index) => (
                <option key={org.name} value={org.id}>
                  {org.name}
                </option>
              ))}
          </select>

          <button className='button primary' onClick={() => this.openModal({})}>
            Create Resource
          </button>
        </div>
        {this.state.modalVisible && (
          <div className='form'>
            <form onSubmit={this.submitHandler}>
              <ul className='form-container'>
                <li>
                  <h2>
                    {' '}
                    {this.state._id ? 'Update Resource' : 'Create Resource'}
                  </h2>
                </li>
                {/* <li>
                    {loadingSave && <div>Loading...</div>}
                    {errorSave && <div>{errorSave}</div>}
                  </li> */}

                <li>
                  <label htmlFor='name'>Name</label>
                  <input
                    type='text'
                    name='name'
                    value={this.state.name}
                    id='name'
                    onChange={(e) => this.setState({ name: e.target.value })}
                  ></input>
                </li>
                <li>
                  <label htmlFor='description'>Description</label>
                  <textarea
                    name='description'
                    value={this.state.description}
                    id='description'
                    onChange={(e) =>
                      this.setState({ description: e.target.value })
                    }
                  ></textarea>
                </li>
                <li>
                  <label htmlFor='name'>Category</label>
                  <input
                    type='text'
                    name='category'
                    value={this.state.category}
                    id='category'
                    onChange={(e) =>
                      this.setState({ category: e.target.value })
                    }
                  ></input>
                </li>
                <li>
                  <label htmlFor='price'>Price</label>
                  <input
                    type='text'
                    name='price'
                    value={this.state.fieldDisplayPrice}
                    id='price'
                    onChange={this.handlePriceChange}
                  ></input>
                </li>
                <li>
                  <label htmlFor='currency'>Currency</label>
                  <select
                    name='currency'
                    value={
                      this.state.currency === '' ? 'ALGO' : this.state.Currency
                    }
                    id='currency'
                    onChange={(e) =>
                      this.setState({ currency: e.target.value })
                    }
                  >
                    <option key='USDC' value='USDC'>
                      USDC
                    </option>
                    <option key='ALGO' value='ALGO'>
                      ALGO
                    </option>
                  </select>
                </li>
                <li>
                  <label htmlFor='countInStock'>CountInStock</label>
                  <input
                    type='text'
                    name='countInStock'
                    value={this.state.countInStock}
                    id='countInStock'
                    onChange={(e) =>
                      this.setState({ countInStock: e.target.value })
                    }
                  ></input>
                </li>

                <li>
                  <label htmlFor='location' style={{ fontWeight: 'bold' }}>
                    Location
                  </label>
                </li>
                <li>
                  <label htmlFor='address'>Street Address</label>
                  <textarea
                    name='address'
                    value={this.state.location.address}
                    id='address'
                    onChange={this.handleLocChange}
                  ></textarea>
                </li>
                <li>
                  <label htmlFor='city'>City</label>
                  <textarea
                    name='city'
                    value={this.state.location.city}
                    id='city'
                    onChange={this.handleLocChange}
                  ></textarea>
                </li>
                <li>
                  <label htmlFor='postalCode'>Postal (zip) Code</label>
                  <textarea
                    name='postalCode'
                    value={this.state.location.postalCode}
                    id='postalCode'
                    onChange={this.handleLocChange}
                  ></textarea>
                </li>
                <li>
                  <label htmlFor='country'>Country</label>
                  <textarea
                    name='country'
                    value={this.state.location.country}
                    id='country'
                    onChange={this.handleLocChange}
                  ></textarea>
                </li>

                <li>
                  <label htmlFor='condition'>Condition</label>
                  <select
                    name='condition'
                    value={this.state.condition}
                    id='condition'
                    onChange={(e) =>
                      this.setState({ condition: e.target.value })
                    }
                  >
                    <option key='NEW' value='NEW'>
                      New
                    </option>
                    <option key='USED' value='USED'>
                      Used
                    </option>
                  </select>
                </li>
                <li>
                  <label htmlFor='supplier'>Supplier</label>
                  <FormSelect
                    name='orgId'
                    onChange={this.handleOrgChange}
                    value={this.state.selectedOrg}
                  >
                    {this.props.person &&
                      this.state.orgs.map((org, index) => (
                        <option key={index} value={org.id}>
                          {org.name}
                        </option>
                      ))}
                  </FormSelect>
                </li>
                <li>
                  <label htmlFor='shippingOption'>Shipping Option</label>
                  <select
                    name='shippingOption'
                    value={this.state.shippingOption}
                    id='shippingOption'
                    onChange={(e) =>
                      this.setState({ shippingOption: e.target.value })
                    }
                  >
                    <option key='PICKUP' value='PICKUP'>
                      Pickup
                    </option>
                    <option key='DELIVERY' value='DELIVERY'>
                      Delivery
                    </option>
                  </select>
                </li>
                <li>
                  <label htmlFor='brand'>Brand</label>
                  <input
                    type='text'
                    name='brand'
                    value={this.state.brand}
                    id='brand'
                    onChange={(e) => this.setState({ brand: e.target.value })}
                  ></input>
                </li>
                {/* Option to upload image only on edit window */}
                {this.state._id ? (
                  <li>
                    <ImageUpload
                      resourceId={this.state._id}
                      authHelper={this.props.authHelper}
                      updateImage={this.updateResourceImageStateFromChild}
                    />
                  </li>
                ) : (
                  ''
                )}
                <row>
                  <div style={{ display: 'flex', justifyContent: 'center' }}>
                    <button type='submit' className='button primary'>
                      {this.state._id ? 'Update' : 'Create'}
                    </button>
                    {'  '}
                    <button
                      type='button'
                      onClick={() => this.setModalVisible(false)}
                      className='button primary'
                    >
                      Cancel
                    </button>
                  </div>
                </row>
              </ul>
            </form>
          </div>
        )}

        <div className='resource-list'>
          <table className='table'>
            <thead>
              <tr>
                <th>Visibility</th>
                <th>Action</th>
                <th>ID</th>
                <th>Name</th>
                <th>Price</th>
                <th>Currency</th>
                <th>Category</th>
                <th>Brand</th>
                <th>Supplier/Org</th>
              </tr>
            </thead>
            <tbody>{this.displayselectedResources(this.state.selected)}</tbody>
          </table>
        </div>
      </div>
    )
  }
}
export default ResourcesScreen
