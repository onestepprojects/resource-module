import React from 'react'
import { Link } from 'react-router-dom-v5'
import { Card, Row } from 'shards-react'
import { detailsResource } from '../api/resourceApiCalls'
import { getOrder } from '../api/orderApiCalls'
import { getOrg } from '../api/orgApiCalls'

import numeral from 'numeral'

const ALGO_USDC_SCALE = 1 / 1000000

class OrderScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      id: this.props.match.params.id,
      resource: {
        shippingOption: 'LOADING...',
        location: {
          lat: null,
          long: null,
          address: null,
          city: null,
          country: null,
          postalCode: null
        },
        reviews: []
      },
      qty: 1,
      supplierName: '',
      order: {}
    }
  }

  componentDidMount() {
    if (this.props.authHelper) {
      // get order
      getOrder(this.props.authHelper, this.state.id).then((res) => {
        if (res.status < 200 || res.status >= 300) {
          alert(res.data.message)
        } else {
          this.setState({ order: res.data })

          // Get all the resources to show as well
          detailsResource(
            this.props.authHelper,
            res.data.resource,
            (res) => this.setState({ resource: res.data }), // Call back method used to set the state
            (err) => {
              alert(err)
            } // Error call back method used to alert error.
          )

          // gets org name
          getOrg(this.props.authHelper, res.data.orgId).then((res) => {
            if (res.status < 200 || res.status >= 300) {
              this.setState({ supplierName: 'Supplier organization not found' })
            } else {
              this.setState({ supplierName: res.data.name })
            }
          })
        }
      })
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.authHelper !== prevProps.authHelper) {
      // get order
      getOrder(this.props.authHelper, this.state.id).then((res) => {
        if (res.status < 200 || res.status >= 300) {
          alert(res.data.message)
        } else {
          this.setState({ order: res.data })

          // Get all the resources to show as well
          detailsResource(
            this.props.authHelper,
            this.state.id,
            (res) => this.setState({ resource: res.data }), // Call back method used to set the state
            (err) => {
              alert(err)
            } // Error call back method used to alert error.
          )

          // gets org name
          getOrg(this.props.authHelper, this.state.resource.orgId).then(
            (res) => {
              if (res.status < 200 || res.status >= 300) {
                this.setState({
                  supplierName: 'Supplier organization not found'
                })
              } else {
                this.setState({ supplierName: res.data.name })
              }
            }
          )
        }
      })
    }
  }

  render() {
    const resource = this.state.resource

    return (
      <div>
        <div className='details'>
          <Row>
            <div className='details-image'>
              <img src={resource.image} alt='resource'></img>
            </div>
            <Card className='details-info'>
              <div className='details-info-inner'>
                <ul>
                  <li>
                    <h3>{resource.name}</h3>
                  </li>
                  <li>
                    <b>
                      {numeral(resource.price * ALGO_USDC_SCALE).format(
                        '0,0.00'
                      )}{' '}
                      {resource.currency}
                    </b>
                  </li>
                  <li>
                    <h6 className='mb-0'>Order State</h6>
                    <div>{this.state.order.orderState}</div>
                  </li>
                  <li>
                    <h6 className='mb-0'>Supplier</h6>
                    <div>
                      <Link
                        to={`${this.props.mountPath}/supplier/${resource.supplierId}`}
                      >
                        {this.state.supplierName}
                      </Link>
                    </div>
                  </li>
                </ul>
              </div>
            </Card>
          </Row>
        </div>
      </div>
    )
  }
}

export default OrderScreen
