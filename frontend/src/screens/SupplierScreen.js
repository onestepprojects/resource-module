import React from 'react'
import { Card, Row } from 'shards-react'
import { getSupplier, saveSupplierReview } from '../api/supplierApiCalls'
import { getOrg } from '../api/orgApiCalls'
import Rating from '../components/Rating'

class SupplierScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      id: this.props.match.params.id,
      supplier: { reviews: [] },
      org: null,
      rating: ''
    }
  }

  reviewsubmitHandler(e) {
    e.preventDefault()
    saveSupplierReview(this.props.authHelper, this.state.id, {
      rating: this.state.rating,
      comment: this.state.comment
    })
    window.location.reload()
  }

  submitHandler(e) {
    e.preventDefault()
  }

  // loads supplier from resource module first, then uses org ID to load org data from org module
  componentDidMount() {
    getSupplier(this.props.authHelper, this.state.id).then((res) => {
      if (res.status < 200 || res.status >= 300) {
        alert(res.data.message)
      } else {
        // set supplier
        this.setState({ supplier: res.data })

        // get org info
        getOrg(this.props.authHelper, res.data.orgId).then((res) => {
          if (res.status < 200 || res.status >= 300) {
            alert(res.data.message)
          } else {
            this.setState({ org: res.data })
          }
        })
      }
    })
  }

  render() {
    return (
      <div>
        {this.state.org && (
          <div className='details'>
            <Row>
              <div className='details-image'>
                <img src={this.state.org.logo.url} alt='logo'></img>
              </div>
              <Card className='details-info'>
                <div className='details-info-inner'>
                  <ul>
                    <li>
                      <h3>{this.state.org.name}</h3>
                    </li>
                    <li>
                      <a href='#reviews'>
                        <Rating
                          value={this.state.supplier.rating}
                          text={this.state.supplier.numReviews + ' reviews'}
                        />
                      </a>
                    </li>
                    <li>
                      <b>{this.state.org.tagline}</b>
                    </li>
                    <li>
                      <h6 className='mb-0'>Description</h6>
                      <div>{this.state.org.description.description}</div>
                    </li>
                    <li>
                      <h6 className='mb-0'>Website</h6>
                      <div>
                        <a
                          target='_blank'
                          href={this.state.org.website}
                          rel='noreferrer'
                        >
                          {this.state.org.website}
                        </a>
                      </div>
                    </li>
                    <li>{/* Space for something here in the future */}</li>
                  </ul>
                </div>
              </Card>
            </Row>
            <Row>
              <div className='reviewscard'>
                <Card medium className='mb-8'>
                  <div className='reviewsheader'>Reviews</div>

                  {!this.state.supplier.reviews.length && (
                    <div>There is no review</div>
                  )}
                  <ul className='review' id='reviews'>
                    {this.state.supplier.reviews.map((review) => (
                      <li key={review._id}>
                        <div id='reviewperson'>{review.name} </div>
                        <div id='reviewtime'>
                          {review.createdAt.substring(0, 10)}
                        </div>
                        <div>
                          <Rating value={review.rating}></Rating>
                        </div>

                        <div id='reviewcomments'>{review.comment}</div>
                      </li>
                    ))}
                    <li>
                      <h6>Write a customer review</h6>
                      {this.props.person ? (
                        <form onSubmit={this.reviewsubmitHandler}>
                          <ul className='form-container'>
                            <li>
                              <label htmlFor='rating'>Rating</label>
                              <select
                                name='rating'
                                id='rating'
                                value={this.state.rating}
                                onChange={(e) =>
                                  this.setState({ rating: e.target.value })
                                }
                              >
                                <option value='0'></option>
                                <option value='1'>1 - Poor</option>
                                <option value='2'>2 - Fair</option>
                                <option value='3'>3 - Good</option>
                                <option value='4'>4 - Very Good</option>
                                <option value='5'>5 - Excelent</option>
                              </select>
                            </li>
                            <li>
                              <label htmlFor='comment'>Comment</label>
                              <textarea
                                className='comment-text'
                                name='comment'
                                value={this.state.comment}
                                onChange={(e) =>
                                  this.setState({ comment: e.target.value })
                                }
                              ></textarea>
                            </li>
                            <li>
                              <button type='submit' className='button primary'>
                                Submit
                              </button>
                            </li>
                          </ul>
                        </form>
                      ) : (
                        ''
                      )}
                    </li>
                  </ul>
                </Card>
              </div>
            </Row>
          </div>
        )}
      </div>
    )
  }
}

export default SupplierScreen
