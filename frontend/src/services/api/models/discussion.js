import { graphQl, post } from '../api'

export async function getComments(authHelper, id) {
  console.log(id)
  return graphQl(
    authHelper,
    `{
      nodes(filter: {schema: {is: discussion}, fields: {discussion: {id: {equals: "${id}"}}}}) {
        elements {
          ... on discussion {
            uuid
            fields {
              id
              comments {
                ... on comment {
                  uuid
                  created
                  creator {
                    uuid
                    firstname
                    username
                  }
                  fields {
                    value
                  }
                }
              }
            }
          }
        }
      }
    }`
  )
}

export async function getDiscussion(authHelper, id) {
  console.log('Method Called')
  console.log(id)
  return graphQl(
    authHelper,
    `{
      nodes(filter: {schema: {is: discussion}, fields: {discussion: {id: {equals: "${id}"}}}}) {
        elements {
          ... on discussion {
            uuid
              }
            }
          }
				}`
  )
}

export async function newPost(authHelper, text) {
  var id = Math.random().toString(36).substring(2, 15)

  return post(authHelper, `/resources/webroot/comments/${id}`, {
    language: 'en',
    schema: {
      name: 'comment'
    },
    fields: {
      id: id,
      value: text
    }
  }).then((data) => {
    return { uuid: data.uuid, language: data.language }
  })
}

export async function addPostToDiscussion(
  authHelper,
  uuid,
  boardUuid,
  comments
) {
  if (!comments) {
    comments = [{ uuid: uuid }]
  } else {
    comments = [...comments, { uuid: uuid }]
  }

  return post(authHelper, `/resources/nodes/${boardUuid}`, {
    language: 'en',
    fields: {
      comments: comments
    }
  })
}

export async function createDiscussion(authHelper, id, name) {
  const randString = Math.random().toString(36).substring(2, 15)
  console.log

  return post(authHelper, `/resources/webroot/${id}`, {
    language: 'en',
    schema: {
      name: 'discussion'
    },
    fields: {
      id: id,
      name: name
    }
  }).then((data) => {
    if (!data) {
      throw new Error('failure')
    } else {
      return { uuid: data.uuid, language: data.language }
    }
  })
}

export default createDiscussion
