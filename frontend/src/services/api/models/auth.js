import { get } from '../api'

export async function getCurrentUser(auth) {
  return get(auth, `/auth/me`)
}

export async function getUserById(auth, uuid) {
  return get(auth, `/users/${uuid}`)
}
