/* eslint-disable no-undef */

const apiUrl = `${process.env.REACT_APP_SOLUTIONS_API_URL}` //https://solutions/onesteprelief.org/api/v2
const projectName = 'resources'

export function graphQl(auth, query, variables) {
  return post(auth, `/${projectName}/graphql`, {
    query,
    variables
  }).catch((response) => response.data)
}

export async function get(auth, path, headers = {}) {
  let token = await auth.GetUserIdToken()
  return fetch(`${apiUrl}${path}`, {
    headers: { ...headers, Authorization: `Bearer ${token}` }
  }).then((response) => response.json())
}

export async function get_file(auth, path) {
  let token = await auth.GetUserIdToken()
  return fetch(`${apiUrl}${path}`, {
    headers: { 'Content-Type': 'blob', Authorization: `Bearer ${token}` }
  }).then((response) => response.blob())
}

export function get_base(path, headers = {}) {
  return fetch(`${apiUrl}${path}`, {
    headers: headers
  }).catch((response) => response.json())
}

export async function post(auth, path, data) {
  let token = await auth.GetUserIdToken()

  return fetch(`${apiUrl}${path}`, {
    body: JSON.stringify(data),
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`
    }
  }).then((response) => response.json())
}
