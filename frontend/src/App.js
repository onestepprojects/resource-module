import React from 'react'
import { BrowserRouter, Route, Link } from 'react-router-dom-v5'
import './App.css'
import HomeScreen from './screens/HomeScreen'
import ResourceScreen from './screens/ResourceScreen'
import BuyScreen from './screens/BuyScreen'
import SigninScreen from './screens/SigninScreen'
import { useSelector } from 'react-redux'
import RegisterScreen from './screens/RegisterScreen'
import ResourcesScreen from './screens/ResourcesScreen'
import ShippingScreen from './screens/ShippingScreen'
import PaymentScreen from './screens/PaymentScreen'
import PlaceOrderScreen from './screens/PlaceOrderScreen'
import OrderScreen from './screens/OrderScreen'
import ProfileScreen from './screens/ProfileScreen'
import OrdersScreen from './screens/OrdersScreen'
import Discussions from './components/Discussions'
import { DefaultLayout } from './layouts'
import SupplierPrefScreen from './screens/SupplierPrefScreen'

import Amplify, { Auth } from 'aws-amplify'
import awsconfig from './aws-exports'

Amplify.configure(awsconfig)

// >>New - Configuring Auth Module
Auth.configure(awsconfig)

function App() {
  const userSignin = useSelector((state) => state.userSignin)
  const { userInfo } = userSignin

  const openMenu = () => {
    document.querySelector('.sidebar').classList.add('open')
  }
  const closeMenu = () => {
    document.querySelector('.sidebar').classList.remove('open')
  }
  return (
    <BrowserRouter>
      <DefaultLayout>
        {/* <div className="grid-container"> */}
        <header className='header'>
          <div className='brand'>
            {/* <button onClick={openMenu}>&#9776;</button>
            <Link to="/">amazona</Link> */}
          </div>
          <div className='header-links'>
            <a href='/discussions'>Discussions</a>
            {userInfo ? (
              <Link to='/profile'>{userInfo.name}</Link>
            ) : (
              <Link to='/signin'>Sign In</Link>
            )}
            {userInfo && userInfo.isAdmin && (
              <div className='dropdown'>
                <a href='#'>Supplier</a>
                <ul className='dropdown-content'>
                  <li>
                    <Link to='/orders'>Orders</Link>
                    <Link to='/resources'>Resources</Link>
                    <Link to='/supplierPrefs'>Preferences</Link>
                  </li>
                </ul>
              </div>
            )}
          </div>
        </header>
        {/* <aside className="sidebar">
          <h3>Shopping Categories</h3>
          <button className="sidebar-close-button" onClick={closeMenu}>
            x
          </button>
          <ul className="categories">
            <li>
              <Link to="/category/Pants">Pants</Link>
            </li>

            <li>
              <Link to="/category/Shirts">Shirts</Link>
            </li>
          </ul>
        </aside> */}
        <main className='main'>
          <div className='content'>
            <Route
              path={`${match.url}/discussions/:id`}
              component={Discussions}
            />
            <Route path='/orders' component={OrdersScreen} />
            <Route path='/profile' component={ProfileScreen} />
            <Route path='/order/:id' component={OrderScreen} />
            <Route
              path='/resources'
              render={() => <ResourcesScreen selected='All' />}
            />
            <Route path='/shipping' component={ShippingScreen} />
            <Route path='/payment' component={PaymentScreen} />
            <Route path='/placeorder' component={PlaceOrderScreen} />
            <Route path='/signin' component={SigninScreen} />
            <Route path='/register' component={RegisterScreen} />
            <Route path='/resource/:id/:qty' component={ResourceScreen} />
            <Route path='/buy/:id' component={BuyScreen} />
            <Route path='/category/:id' component={HomeScreen} />
            <Route path='/' exact={true} component={HomeScreen} />
            <Route path='/supplierPrefs' component={SupplierPrefScreen} />
          </div>
        </main>
        {/* <footer className="footer">All right reserved.</footer> */}
        {/* </div> */}
      </DefaultLayout>
    </BrowserRouter>
  )
}

export default App
